import handleResetEntity from 'util/HandleResetEntity';
import handleResetMessages from 'util/HandleResetMessages';
import handleResetForm from 'util/HandleResetForm';

const handleCancelFlow = dispatch => {
	handleResetEntity(dispatch, 'buscarCliente', 'contrato', '');
	handleResetEntity(dispatch, 'buscarCliente', 'nombreCliente', '');
	handleResetEntity(dispatch, 'buscarCliente', 'cliente', {});
	handleResetEntity(dispatch, 'buscarCliente', 'existeCliente', false);
	handleResetMessages(dispatch);
	handleResetForm(dispatch, 'BUSCAR_CLIENTE');
};

export default handleCancelFlow;
