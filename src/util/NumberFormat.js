import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

const handleAllowZero = (floatValue, allowZero) => {
	return allowZero && floatValue === 0;
};
const handleMaxAmount = (floatValue, maxAmount) => {
	return floatValue <= maxAmount;
};
const handleAllowEmpty = (formattedValue, allowEmpty) => {
	return allowEmpty && formattedValue === '';
};
const handleAllowPoint = formattedValue => {
	return formattedValue === '.';
};

const NumberFormatCustom = props => {
	const {
		inputRef,
		name,
		decimalScale = 2,
		maxAmount,
		allowZero,
		allowEmpty,
		onChange,
		...other
	} = props;
	const isAllowed = values => {
		const { formattedValue, floatValue } = values;
		const validAllowEmpty = handleAllowEmpty(formattedValue, allowEmpty);
		const validMaxAmount = handleMaxAmount(floatValue, maxAmount);
		const validAllowZero = handleAllowZero(floatValue, allowZero);
		const validDecimal = handleAllowPoint(formattedValue);
		return validAllowEmpty || validMaxAmount || validAllowZero || validDecimal;
	};
	return (
		<NumberFormat
			{...other}
			allowEmptyFormatting
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.floatValue,
						name,
					},
				});
			}}
			isAllowed={isAllowed}
			thousandSeparator
			allowNegative={false}
			decimalScale={decimalScale}
		/>
	);
};

NumberFormatCustom.propTypes = {
	inputRef: PropTypes.func.isRequired,
	onChange: PropTypes.func.isRequired,
};

export default NumberFormatCustom;
