import React, { Suspense } from 'react';
import mainMenu from 'constants/Menu';
import DynamicImport from 'constants/DynamicImport';
import { findSubMenuItem } from 'util/parseKeycodemenu';
import NoMenu from 'containers/NoMenu';

const MainContainer = props => {
	const { params } = props;
	let Component = null;
	const { subpage } = params;

	const subMenuItem = findSubMenuItem(mainMenu, 'path', subpage);
	const componentPath = `containers/${subMenuItem.parentComponentName}/${subMenuItem.componentName}/${subMenuItem.componentName}`;
	Component = DynamicImport(componentPath);
	return (
		<Suspense>
			<Component
				{...props}
				{...subMenuItem}
			/>
		</Suspense>
	);
};

export default MainContainer;