import { hideNotification } from 'actions/notification.actions';
import handleNotification from 'util/HandleNotification';

const handleCancel = (dispatch, feature) => {
	dispatch(hideNotification(feature));
};

const handleFinalAmountError = dispatch => {
	handleNotification(dispatch, {
		type: 'error',
		okButtonTitle: 'Cerrar',
		content: 'Error al actualizar el monto en caja',
		onOk: () => handleCancel(dispatch, 'GALILEO'),
		feature: 'GALILEO',
		autoFocus: true,
	});
};

export default handleFinalAmountError;
