const parseDenominacion = list => {
	if (list === 0) {
		return;
	}
	return list.map(item => {
		return {
			id: `$${item.descripcion}`,
			codigo: item.codigo,
			value: Number(item.descripcion),
			label: `$${item.descripcion}`,
			name: `denominacion${item.descripcion}`,
		};
	});
};

export default parseDenominacion;
