/* eslint-disable no-console */
export const findIndexMenu = (menu, searchCriterion, searchValue) => {
	return menu.findIndex(item => item[searchCriterion] === searchValue);
};
export const findSubMenu = (mainMenu = [], searchCriterion = '', searchValue = '') => {
	return mainMenu.find(menu => menu[searchCriterion] === searchValue).routes;
};
export const findSubMenuItem = (mainMenu = [], searchCriterion = '', searchValue = '') => {
	return mainMenu
		.map(menu => {
			return menu.routes.find(route => route[searchCriterion] === searchValue);
		})
		.find(menuSelected => menuSelected !== undefined);
};
export const findSubmenuItemSelected = (mainMenu, modifierState, key) => {
	const keyCombination = `${modifierState}+${key}`;
	const subMenuSelectedParentIndex = mainMenu.findIndex(item =>
		item.routes.find(route => route.keyCombination === keyCombination)
	);
	const subMenuSelected = mainMenu
		.map(menu => menu.routes.find(route => route.keyCombination === keyCombination))
		.find(menuSelected => menuSelected !== undefined);

	// eslint-disable-next-line no-extra-boolean-cast
	if (Boolean(subMenuSelected)) {
		return { subMenuSelected, subMenuSelectedParentIndex };
	}
	console.error(`No existe la combinación ${keyCombination}`);
};
