import moment from 'moment';
import currency from 'currency.js';

export const toNumber = value => {
	return Number(value);
};

export const toCurrency = value => {
	return value.toLocaleString('es-MX');
};

export const currencyStringToNumber = value => {
	const fromString = String(value);
	return Number(fromString.replace(/[^0-9\.]+/g, ''));
};

export const currencyNumberToString = value => {
	const fromString = String(value);

	if (typeof Number(fromString) === 'number' && Number.isFinite(Number(fromString))) {
		return currency(fromString, { formatWithSymbol: true, separator: ',' }).format();
	}
};
export const stringToPercentage = value => {
	const fromString = value.toString();
	if (typeof Number(fromString) === 'number' && Number.isFinite(Number(fromString))) {
		return `% ${fromString}`;
	}
};
export const toCamelCase = word => {
	return word
		.replace(/\./g, '')
		.toLowerCase()
		.replace(/\s(.)/g, $1 => {
			return $1.toUpperCase();
		})
		.replace(/\s/g, '')
		.replace(/^(.)/, $1 => {
			return $1.toLowerCase();
		})
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, '');
};

export const getDateFormat = (value, format = '') => {
	let newValue = '';
	const validFormat = [
		'DD-MM-YYYY',
		'DD-MM-YY',
		'DD/MM/YYYY',
		'DD/MM/YY',
		'YY-MM-DD',
		'YY/MM/DD',
		'YYYY/MM/DD',
		'YYYY-MM-DD',
		'YYYY/MMM/DD',
		'YYYY-MMM-DD',
		'YYYY-MM-DD h:m:s.sss',
		'YYYY-MM-DD h:m:s.ss',
		'YYYY-MM-DD h:m:s.s',
		'YYYY-MM-DD h:m:s.SSS',
		'YYYY-MM-DD h:m:s.SS',
		'YYYY-MM-DD h:m:s.S',
		'YYYY-MM-DD h:mm:ss.sss',
		'YYYY-MM-DD h:mm:ss.ss',
		'YYYY-MM-DD h:mm:ss.s',
		'YYYY-MM-DD h:mm:ss.SSS',
		'YYYY-MM-DD h:mm:ss.SS',
		'YYYY-MM-DD h:mm:ss.S',
		'YYYY-MM-DD HH:mm:ss.sss',
		'YYYY-MM-DD HH:mm:ss.ss',
		'YYYY-MM-DD HH:mm:ss.s',
		'YYYY-MM-DD HH:mm:ss.SSS',
		'YYYY-MM-DD HH:mm:ss.SS',
		'YYYY-MM-DD HH:mm:ss.S',
		'YYYY-MM-DD HH:mm:ss.ssssss',
		'YYYY-MM-DD HH:mm:ss',
	];

	if (!value) {
		newValue = 'Sin fecha';
	}
	switch (typeof value) {
		case 'object':
			newValue = moment(value).format(format || 'DD/MM/YYYY');
			break;

		case 'string':
			newValue =
				moment(value, validFormat, true).isValid() &&
				moment(value, validFormat, true).format(format || 'DD/MM/YYYY');
			break;

		default:
			newValue = moment(value).format(format || 'DD/MM/YYYY');
			break;
	}
	if (!moment(value, validFormat, true).isValid()) {
		newValue = 'Fecha no válida';
	}

	return newValue;
};
export const setDateFormat = fecha => {
	let fechaFormat;
	if (fecha === null) {
		fechaFormat = '';
	} else {
		fechaFormat = moment(fecha).format('YYYY-MM-DD');
	}
	return fechaFormat;
};
export const getContentFormat = (content, { type = '', format = '' }) => {
	let newValue = '';
	if (typeof type !== 'object') {
		if (type && type === 'date') {
			newValue = getDateFormat(content, format);
		}
		if (type && type === 'currency') {
			newValue = currencyNumberToString(content);
		}
		if (type && type === 'percentage') {
			const fromString = content.toString();
			newValue = `${fromString}%`;
		}
		if (!type) {
			newValue = content;
		}
		return newValue;
	}
	newValue = content;
	return newValue;
};

export const parseContrato = contrato => {
	return contrato.padStart(12, '0');
};
