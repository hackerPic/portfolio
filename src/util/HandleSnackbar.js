import { showSnackbar } from 'actions/snackbar.actions';

const handleSnackbar = (dispatch, props) => {
	const { message, feature, type, autoHideDuration } = props;
	dispatch(
		showSnackbar({
			payload: {
				open: true,
				message: message && message,
				feature: feature && feature,
				type: type && type,
				autoHideDuration: autoHideDuration && autoHideDuration,
			},
		})
	);
};

export default handleSnackbar;
