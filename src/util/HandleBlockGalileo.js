import NOTIFICATION_MESSAGES from 'constants/notificationMessages';
import { hideNotification } from 'actions/notification.actions';
import handleNotification from 'util/HandleNotification';

const handleRedirect = (dispatch, push, feature) => {
	dispatch(hideNotification(feature));
	push('/administracion/traspaso-caja-seguridad');
};

const handleBlockGalileo = (dispatch, push, feature) => {
	handleNotification(dispatch, {
		content: 'Lamentamos el inconveniente',
		children: NOTIFICATION_MESSAGES.blockGalileo,
		onOk: () => handleRedirect(dispatch, push, feature),
		feature,
		autoFocus: true,
	});
};
export default handleBlockGalileo;
