const handleQueryRequest = (headerRequest, requestData, query, idOperadorAutoriza) => {
	const request = {
		headerRequest: {
			...headerRequest,
			idOperadorAutoriza,
		},
		...requestData,
	};
	return {
		...query,
		data: request,
	};
};

export default handleQueryRequest;
