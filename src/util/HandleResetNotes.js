import handleResetEntity from 'util/HandleResetEntity';

const handleResetNotes = dispatch => {
	handleResetEntity(dispatch, 'pagos', 'payOff', 0);
	handleResetEntity(dispatch, 'pagos', 'toggleNotes', false);
	handleResetEntity(dispatch, 'pagos', 'getNotesCall', false);
	handleResetEntity(dispatch, 'pagos', 'tooglePayoff', false);
	handleResetEntity(dispatch, 'pagos', 'notes', []);
	handleResetEntity(dispatch, 'pagos', 'notesCatalogue', []);
};

export default handleResetNotes;
