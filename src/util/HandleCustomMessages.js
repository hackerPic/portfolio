import { addCustomMessages } from 'actions/messages.actions';

const handleCustomMessages = (dispatch, mensajes) => {
	const customMessages = mensajes.map(mensaje => {
		return {
			descripcionMensaje: mensaje.descripcionMensaje,
			negrita: !!mensaje.negrita,
			cursiva: !!mensaje.cursiva,
			subrayado: !!mensaje.subrayado,
			prioridad: mensaje.prioridad,
		};
	});

	dispatch(addCustomMessages(customMessages));
};

export default handleCustomMessages;
