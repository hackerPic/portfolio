import { hideNotification } from 'actions/notification.actions';
import handleNotification from 'util/HandleNotification';

const handleCancel = (dispatch, feature) => {
	dispatch(hideNotification(feature));
};

const handleTicketError = (dispatch, feature) => {
	handleNotification(dispatch, {
		okButtonTitle: 'Cerrar',
		content: 'Hubo un error con el ticket',
		children:
			'La operación se aplicó correctamente, pero no se obtuvieron los datos del ticket para la impresión.',
		onOk: () => handleCancel(dispatch, feature),
		feature,
		autoFocus: true,
	});
};
export default handleTicketError;
