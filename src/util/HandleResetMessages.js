import { setMessages } from 'actions/messages.actions';

const handleResetMessages = dispatch => {
	dispatch(setMessages([]));
};

export default handleResetMessages;
