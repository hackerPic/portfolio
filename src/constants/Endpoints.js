/* -------CONSULTA MAPEO EMPRESAS---- */
export const CONSULTAR_MAPEO_EMPRESAS = '/cxf/seguridad/autorizacion/consultarMapeoEmpresas';
/* -------CONSULTAR FACULTADES------- */
export const CONSULTAR_USUARIO_EMAIL = '/cxf/seguridad/autorizacion/consultarUsuarioEmail';
export const CONSULTAR_USUARIO_ACTIVO = '/cxf/empleados/rest/buscarActivosSirh';
export const CONSULTAR_USUARIO_FACULTADES = '/cxf/seguridad/autorizacion/consultarUsuarios';
/* -------AUTORIZA------- */
export const AUTORIZA_PERSONA_FACULTAD = '/cxf/seguridad/autorizacion/autorizaPersonaFacultad';
/* -------OFICINAS------- */
export const CONSULTA_OFICINAS_POST = '/AdminEfectivoWSRest/rest/consultasRest/consultaOficina';
/**
 * -------BUSCAR CLIENTE-------
 */
export const CONSULTA_CLIENTES_NOMBRE_POST =
	'/AdminCarteraWSRest/rest/consultasRest/consultarClientes';

/* Disposiciones masivas */
export const CONSULTA_DISPOSICIONES_MASIVAS_POST =
	'msk/operaciones-centrales/rest/consultarDisposicionMasiva';
export const APLICAR_DISPOSICIONES_MASIVAS_POST =
	'msk/operaciones-centrales/rest/aplicarDisposicionMasiva';

/* OBTENER_CATEGORIAS_F2 */
export const OBTENER_CATEGORIAS_F2_GET = 'msk/servicios-f2/rest/catalogos/categoria';
/* OBTENER SUBCATEGORIAS_F2 */
export const OBTENER_SUBCATEGORIAS_F2_GET = 'msk/servicios-f2/rest/catalogos/subcategoria';
/* OBTENER CONTRATOS GENERAL */
export const OBTENER_CONTRATOS_GENERAL_F2_GET = 'msk/servicios-f2/rest/contratos/agrupar';
/* OBTENER CONTRATOS FRECUENCIA */
export const OBTENER_CONTRATOS_FRECUENCUA_F2_GET =
	'msk/servicios-f2/rest/contratos/agruparFrecuencia';
