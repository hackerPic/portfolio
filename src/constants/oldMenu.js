const generalMenuBar = [
    {
        primaryText: "Dashboard",
        url: "/",
        show: true,
        pageTitle: "Dashboard"
    },
    {
        subheader: "ADMINISTRACIÓN"
    },
    // {
    //     primaryText: "Domiciliación",
    //     id: "MASNOMINAFAC004",
    //     url: "/cobranza/domiciliacion",
    //     pageTitle: "Consultar Domiciliación"
    // },
    {
        primaryText: "Corresponsales",
        id: "MASNOMINAFAC004",
        url: "/administracion/corresponsales",
        pageTitle: "Lista de corresponsales"
    },
    {
        primaryText: "Alta de corresponsal",
        id: "MASNOMINAFAC008",
        type: "button",
        pageTitle: "Alta de corresponsal"
    },
    {
        primaryText: "Activar / desactivar corresponsal",
        id: "MASNOMINAFAC009",
        type: "button",
        pageTitle: "Activar / desactivar corresponsal"
    },
    {
        primaryText: "Consultar corresponsal",
        id: "MASNOMINAFAC010",
        type: "button",
        pageTitle: "Consultar corresponsal"
    },
    {
        primaryText: "Editar corresponsal",
        id: "MASNOMINAFAC011",
        type: "button",
        pageTitle: "Editar corresponsal"
    },
    {
        primaryText: "Créditos",
        nestedItems: [
            {
                primaryText: "Activar créditos manualmente",
                id: "MASNOMINAFAC012",
                url: "/administracion/creditos/activar/manual",
                pageTitle: "Activar créditos manualmente"
            },
            {
                primaryText: "Activar créditos con layout",
                id: "MASNOMINAFAC013",
                url: "/administracion/creditos/activar/layout",
                pageTitle: "Activar créditos con layout"
            },
            {
                primaryText: "subir créditos con layout",
                id: "MASNOMINAFAC090",
                type: "button"
            }
        ]
    },
    {
        primaryText: "btn activar",
        id: "MASNOMINAFAC047",
        type: "button",
        pageTitle: "Activar créditos manualmente"
    },
    {
        primaryText: "btn subir archivo",
        id: "MASNOMINAFAC048",
        type: "button",
        pageTitle: "boton subir archivo"
    },
    {
        primaryText: "Bitácora de layouts",
        id: "MASNOMINAFAC014",
        url: "/administracion/layouts/bitacora",
        pageTitle: "Layouts bitácora de archivos"
    },
    {
        primaryText: "buscar bitácora de layouts",
        id: "MASNOMINAFAC091",
        type: "button"
    },
    {
        primaryText: "buscar solicitudes crédito manual",
        id: "MASNOMINAFAC092",
        type: "button"
    },
    {
        primaryText: "botón buscar consulta solicitudes de credito",
        id: "MASNOMINAFAC093",
        type: "button"
    },
    {
        primaryText: "ver detalles consulta solicitudes de credito",
        id: "MASNOMINAFAC094",
        type: "button"
    },
    {
        primaryText: "subir archivos exclusiones",
        id: "MASNOMINAFAC095",
        type: "button"
    },
    {
        primaryText: "add asignación automática",
        id: "MASNOMINAFAC096",
        type: "button"
    },
    {
        primaryText: "movimientos excel",
        id: "MASNOMINAFAC097",
        type: "button"
    },
    {
        primaryText: "creditos activados",
        id: "MASNOMINAFAC098",
        type: "button"
    },
    {
        primaryText: "deposito por cuenta",
        id: "MASNOMINAFAC099",
        type: "button"
    },
    {
        primaryText: "depósitos de los corresponsales",
        id: "MASNOMINAFAC0100",
        type: "button"
    },
    {
        primaryText: "reporte general de cobranza",
        id: "MASNOMINAFAC0101",
        type: "button"
    },
    {
        primaryText: "creditos cerrados",
        id: "MASNOMINAFAC0102",
        type: "button"
    },
    {
        primaryText: "listado de pagos",
        id: "MASNOMINAFAC0103",
        type: "button"
    },
    {
        primaryText: "buró credito diario",
        id: "MASNOMINAFAC0104",
        type: "button"
    },
    {
        primaryText: "inconsistencia en las listas",
        id: "MASNOMINAFAC0105",
        type: "button"
    },
    {
        primaryText: "cartera vigente y vencida por corresponsal",
        id: "MASNOMINAFAC0106",
        type: "button"
    },
    {
        primaryText: "buró credito mensual",
        id: "MASNOMINAFAC0107",
        type: "button"
    },
    {
        primaryText: "moratorios gastos de cobranza",
        id: "MASNOMINAFAC0108",
        type: "button"
    },
    {
        primaryText: "saldo corresponsales",
        id: "MASNOMINAFAC0109",
        type: "button"
    },
    {
        primaryText: "detalle saldo corresponsales",
        id: "MASNOMINAFAC0110",
        type: "button"
    },
    {
        primaryText: "ejecutar saldos a favor",
        id: "MASNOMINAFAC049",
        type: "button"
    },
    {
        primaryText: "Aplicacion de Saldos a Favor",
        id: "MASNOMINAFAC018",
        url: "/administracion/saldos-a-favor",
        pageTitle: "Aplicacion de Saldos a Favor"
    },
    {
        primaryText: "Refinanciamiento",
        nestedItems: [
            {
                primaryText: "Lista de refinanciamiento",
                id: "MASNOMINAFAC017",
                url: "/administracion/lista-de-refinanciamiento",
                pageTitle: "Lista de refinanciamiento"
            },
            {
                primaryText: "Carga de exclusiones",
                id: "MASNOMINAFAC016",
                url: "/administracion/exclusiones/carga-de-archivo",
                pageTitle: "Exclusiones"
            }
        ]
    },
    {
        subheader: "RIESGOS"
    },
    {
        primaryText: "Solicitudes",
        nestedItems: [
            {
                primaryText: "Asignar solicitudes de crédito manual",
                id: "MASNOMINAFAC019",
                url: "/riesgos/asignarCredito/manual",
                pageTitle: "Asignar solicitudes de crédito manual"
            },
            {
                primaryText: "Consulta solicitudes de crédito",
                id: "MASNOMINAFAC005",
                url: "/riesgos/solicitudes/credito",
                pageTitle: "Solicitudes de crédito"
            },
            {
                primaryText: "Consulta solicitudes de crédito por dictaminar",
                id: "MASNOMINAFAC006",
                url: "/riesgos/solicitudes/credito-por-dictaminar",
                pageTitle: "Solicitudes de crédito por dictaminar"
            },
            {
                primaryText: "Consultar buzón de solicitudes",
                id: "MASNOMINAFAC023",
                url: "/riesgos/solicitudes/buzon",
                pageTitle: "Buzón de solicitudes por dictaminar"
            }
        ]
    },
    {
        primaryText: "Reportes",
        nestedItems: [
            {
                primaryText: "Solicitudes por analista",
                id: "MASNOMINAFAC020",
                url: "/riesgos/reportes/solicitudes-por-analista",
                pageTitle: "Reporte solicitudes por analista"
            }
        ]
    },
    {
        primaryText: "Detalles de una solicitud",
        id: "MASNOMINAFAC024",
        type: "button"
    },
    {
        primaryText: "Autorizar solicitud",
        id: "MASNOMINAFAC022",
        type: "button",
        pageTitle: "Autorizar solicitud"
    },
    {
        primaryText: "Asignación automática de solicitudes",
        id: "MASNOMINAFAC021",
        url: "/riesgos/batch-asignacion-solicitudes",
        pageTitle: "Asignación automática de solicitudes"
    },
    {
        primaryText: "Monitor Riesgos",
        id: "MASNOMINAFAC042",
        url: "/riesgos/monitor",
        pageTitle: "Monitor Riesgos"
    },
    {
        subheader: "COBRANZA"
    },
    {
        primaryText: "Btn buscar crédito",
        id: "MASNOMINAFAC050",
        type: "button"
    },
    {
        primaryText: "Icono modificar afiliación",
        id: "MASNOMINAFAC051",
        type: "button"
    },
    {
        primaryText: "btn flotante agregar pago",
        id: "MASNOMINAFAC052",
        type: "button"
    },
    {
        primaryText: "Ver desglose",
        id: "MASNOMINAFAC053",
        type: "button"
    },
    {
        primaryText: "Ver bitacora",
        id: "MASNOMINAFAC054",
        type: "button"
    },
    {
        primaryText: "ver detalles",
        id: "MASNOMINAFAC055",
        type: "button"
    },
    {
        primaryText: "btn anular pago",
        id: "MASNOMINAFAC056",
        type: "button"
    },
    {
        primaryText: "btn cargo por incumplimiento",
        id: "MASNOMINAFAC057",
        type: "button"
    },
    {
        primaryText: "Añadir comentario",
        id: "MASNOMINAFAC058",
        type: "button"
    },
    {
        primaryText: "btn simular",
        id: "MASNOMINAFAC059",
        type: "button"
    },
    {
        primaryText: "Estado de cuenta agrupado",
        id: "MASNOMINAFAC060",
        type: "button"
    },
    {
        primaryText: "Historico",
        id: "MASNOMINAFAC061",
        type: "button"
    },
    {
        primaryText: "btn flotante agregar lista de cobro",
        id: "MASNOMINAFAC062",
        type: "button"
    },
    {
        primaryText: "Lista de cobro ver detalles",
        id: "MASNOMINAFAC063",
        type: "button"
    },
    {
        primaryText: "Editar lista de cobro",
        id: "MASNOMINAFAC064",
        type: "button"
    },
    {
        primaryText: "Eliminar lista de cobro",
        id: "MASNOMINAFAC065",
        type: "button"
    },
    {
        primaryText: "Editar afiliación",
        id: "MASNOMINAFAC066",
        type: "button"
    },
    {
        primaryText: "btn aplicar lista cobro",
        id: "MASNOMINAFAC067",
        type: "button"
    },
    {
        primaryText: "btn depósitos lista cobro",
        id: "MASNOMINAFAC068",
        type: "button"
    },
    {
        primaryText: "btn reversar",
        id: "MASNOMINAFAC069",
        type: "button"
    },
    {
        primaryText: "btn nuevo crédito",
        id: "MASNOMINAFAC070",
        type: "button"
    },
    {
        primaryText: "btn exportar archivo",
        id: "MASNOMINAFAC071",
        type: "button"
    },
    {
        primaryText: "ver bitácora",
        id: "MASNOMINAFAC072",
        type: "button"
    },
    {
        primaryText: "clonar",
        id: "MASNOMINAFAC073",
        type: "button"
    },
    {
        primaryText: "Eliminar",
        id: "MASNOMINAFAC074",
        type: "button"
    },
    {
        primaryText: "Subir respuesta",
        id: "MASNOMINAFAC075",
        type: "button"
    },
    {
        primaryText: "Agregar Pld",
        id: "MASNOMINAFAC076",
        type: "button"
    },
    {
        primaryText: "catalogos editar",
        id: "MASNOMINAFAC078",
        type: "button"
    },
    {
        primaryText: "catalogos activarDesactivar",
        id: "MASNOMINAFAC079",
        type: "button"
    },
    {
        primaryText: "catalogos editar estado",
        id: "MASNOMINAFAC081",
        type: "button"
    },
    {
        primaryText: "catalogos activarDesactivar estado",
        id: "MASNOMINAFAC082",
        type: "button"
    },
    {
        primaryText: "ver detalles creditos",
        id: "MASNOMINAFAC088",
        type: "button"
    },
    {
        primaryText: "btn buscar lista cobro",
        id: "MASNOMINAFAC089",
        type: "button"
    },
    {
        primaryText: "Asignar crédito a despacho",
        url: "/cobranza/asignacion-credito-a-despacho",
        pageTitle: "Asignar crédito a despacho"
    },
    {
        primaryText: "Ajustes",
        url: "/cobranza/ajustes",
        pageTitle: "Consulta lista de ajustes"
    },
    /*    {
        primaryText: "Bitácora de control",
        url: "/cobranza/bitacora-control",
        id: "MASNOMINAFAC026",
        pageTitle: "Bitácora de control"
    },*/
    {
        primaryText: "Consultar Crédito",
        url: "/cobranza/consultarCredito",
        pageTitle: "Consultar crédito"
    },
    {
        primaryText: "Créditos",
        url: "/cobranza/creditos",
        id: "MASNOMINAFAC029",
        pageTitle: "créditos"
    },
    {
        primaryText: "Consultar Ajuste",
        url: "/cobranza/consultarAjuste",
        pageTitle: "Consultar ajuste"
    },
    {
        primaryText: "Listas de cobro",
        url: "/cobranza/listas-de-cobro",
        id: "MASNOMINAFAC025",
        pageTitle: "Listas de cobro"
    },
    {
        primaryText: "Reportes Listas PLD",
        url: "/cobranza/reportes-pld",
        id: "MASNOMINAFAC041",
        pageTitle: "Reportes Listas PLD"
    },
    {
        primaryText: "Download Estados de cuenta Masivos",
        url: "/cobranza/download-estados-de-cuenta-masivos",
        pageTitle: "Download Estados de cuenta Masivos"
    },
    {
        primaryText: "Reporte operativo",
        id: "MASNOMINAFAC039",
        url: "/cobranza/operativo",
        pageTitle: "Reporte operativo"
    },
    {
        primaryText: "Procesos Masivos",
        id: "MASNOMINAFAC112",
        url: "/cobranza/procesos-masivos",
        pageTitle: "Procesos Masivos"
    },
    {
        primaryText: "Pagos OXXO",
        id: "MASNOMINAFAC0113",
        id: "MASNOMINAFAC112",
        url: "/cobranza/pagos-oxxo",
        pageTitle: "Pagos OXXO"
    },
    {
        primaryText: "Curso MasNomina",
        id: "MASNOMINAFAC0113",
        id: "MASNOMINAFAC112",
        url: "/cobranza/curso-msnm",
        pageTitle: "Curso Masnomina"
    },
    {
        primaryText: "Consultar tabla de amortización",
        id: "",
        url: "/cobranza/tabla-de-amortizacion",
        pageTitle: "Consultar tabla de amortización"
    },
    {
        primaryText: "Eliminar lista de cobro",
        id: "MASNOMINAFAC035",
        type: "button"
    },
    {
        primaryText: "Reversar lista de cobro",
        id: "MASNOMINAFAC036",
        type: "button"
    },
    {
        primaryText: "Lista de pagos",
        id: "MASNOMINAFAC030",
        type: "button"
    },
    {
        primaryText: "Estado de cuenta",
        id: "MASNOMINAFAC031",
        type: "button"
    },
    {
        primaryText: "Expediente",
        id: "MASNOMINAFAC032",
        type: "button"
    },
    {
        primaryText: "Fallecimiento",
        id: "MASNOMINAFAC033",
        type: "button"
    },
    {
        primaryText: "Venta cartera",
        id: "MASNOMINAFAC034",
        type: "button"
    },
    {
        primaryText: "Cancelar crédito",
        id: "MASNOMINAFAC043",
        type: "button"
    },
    {
        primaryText: "Cerrar crédito",
        id: "MASNOMINAFAC044",
        type: "button"
    },
    {
        primaryText: "Referencia bancaria",
        id: "MASNOMINAFAC045",
        type: "button"
    },
    {
        subheader: "CATÁLOGOS"
    },
    {
        primaryText: "Genericos",
        id: "MASNOMINAFAC001",
        url: "/catalogos/genericos",
        pageTitle: "Catálogos Genéricos"
    },
    {
        primaryText: "btn agregar genericos",
        id: "MASNOMINAFAC077",
        type: "button"
    },
    {
        primaryText: "Estados",
        id: "MASNOMINAFAC002",
        url: "/catalogos/entidades",
        pageTitle: "Catálogos de entidades federativas"
    },
    {
        primaryText: "Giros",
        id: "MASNOMINAFAC003",
        url: "/catalogos/giros",
        pageTitle: "Catálogos de giros"
    },
    {
        primaryText: "btn agregar",
        id: "MASNOMINAFAC080",
        type: "button"
    },
    {
        primaryText: "agregar giro",
        id: "MASNOMINAFAC083",
        type: "button"
    },
    {
        primaryText: "Activar/desactivar",
        id: "MASNOMINAFAC084",
        type: "button"
    },
    {
        primaryText: "Agregar estados",
        id: "MASNOMINAFAC085",
        type: "button"
    },
    {
        primaryText: "Editar estados",
        id: "MASNOMINAFAC086",
        type: "button"
    },
    {
        primaryText: "Activar/desactivar estados",
        id: "MASNOMINAFAC087",
        type: "button"
    },
    {
        primaryText: "Agregar giros",
        id: "MASNOMINAFAC088",
        type: "button"
    },
    {
        subheader: "MONITOR CIERRE"
    },
    {
        primaryText: "Monitor",
        id: "MASNOMINAFAC046",
        url: "/monitor-cierre/monitor",
        pageTitle: "monitor"
    },
    {
        primaryText: "btn inicio",
        id: "MASNOMINAFAC084",
        type: "button"
    },
    {
        primaryText: "btn Finalizar cierre",
        id: "MASNOMINAFAC085",
        type: "button"
    },
    {
        subheader: "CONFIGURACIÓN"
    },
    {
        primaryText: "Jobs",
        id: "MASNOMINAFAC028",
        url: "/configuracion/batch-jobs",
        pageTitle: "Jobs"
    },
    {
        primaryText: "Version 2.1.3",
        id: "MASNOMINAFAC028",
        url: "",
        pageTitle: "Version"
    },
    {
        primaryText: "Editar refinanciamiento",
        id: "MASNOMINAFAC086",
        type: "button"
    },
    {
        primaryText: "Editar sms",
        id: "MASNOMINAFAC087",
        type: "button"
    }
];

export default generalMenuBar;
