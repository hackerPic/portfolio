import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
import  { Redirect } from 'react-router-dom'
import saveToken from './LoginUtils';
import { connect } from "react-redux";
import { getUserProfileState, getUserActiveState, getUserFacultyState } from 'selectors/loginSelector';
import { setLoginUSer, LOGIN_USER } from 'actions/login.actions';
import { getUserFacultiesActions, getActiveUserActions } from './getUserFacultiesActions';
import _ from 'lodash';
import DynamicImport from 'constants/DynamicImport';
const Init = DynamicImport('containers/Layer/Layer');

const RenderBox = ({ click, dis }) => {
    return (
        <div className="msnma-body">
            <div className="msnma-box">
                <form>
                    {/* <div className="msnma-inputBox">
                        <input type="email" name="email" value="" />
                        <label>Username</label>
                    </div>
                    <div className="msnma-inputBox">
                        <input type="text" name="text" value="" />
                        <label>Password</label>
                    </div> */}
                    <button onClick={click} disabled={dis} name="sign-in">Iniciar sesión con Google</button>
                </form>
            </div>
        </div>
    )
};

@connect(store => ({
    loginUser: getUserProfileState(store),
    activeUser: getUserActiveState(store),
    facultyUser: getUserFacultyState(store),
}))
class Login extends Component {

    state = {
        loading: false,
        wasLogued: false,
        faculties: [],
    };

    handleLoginFailure = response => {
        // const { onFailure } = this.props;
        // onFailure(response);
        console.log('error login', response);
    };

    componentDidUpdate = (prevProps, prevState) => {
        const { loginUser, activeUser, facultyUser } = prevProps;
        const { faculties } = prevState;
        const { wasLoged } = this.state;

        if (_.isEmpty(loginUser) && !_.isEmpty(this.props.loginUser) && !wasLoged) {
            this.handleGetGoogleUser(this.props.loginUser.email);
        }

        if (_.isEmpty(activeUser) && !_.isEmpty(this.props.activeUser)) {
            this.handleGetActiveUser(this.props.activeUser);
        }

        if (_.isEmpty(facultyUser) && !_.isEmpty(this.props.facultyUser)) {
            this.handleGetFacultyUser(this.props.facultyUser);
        }

        if (faculties.length === 0 && this.state.faculties.length !== 0) {
            this.handleLogued();
        }

    };

    handleLogued = () => {
        //location.replace('/auth0');
    };

    handleGetGoogleUser = userProfile => {
        const { dispatch } = this.props;
        getUserFacultiesActions(dispatch, userProfile);
    };

    handleGetActiveUser = loginUser => {
        const { dispatch } = this.props;
        getActiveUserActions(dispatch, loginUser);
    };

    handleGetFacultyUser = data => {
        const faculties = Object.values(data);
        this.setState({ faculties });
    };

    handleLoginSuccess = async response => {

        try {
            const { wasLoged } = this.state;
            const { dispatch } = this.props;
            const { tokenId, profileObj } = response;
            if (!wasLoged) {
                dispatch(setLoginUSer(profileObj));
                this.setState({ wasLoged: true });
            }


            saveToken(tokenId);
            this.setState({ loading: true });
        } catch (err) {
            //onFailure(err);
            console.log('error en try', err);
        } finally {
            this.setState({ loading: false });
        }
    };

    render() {
        return (
            <>
            <MenuBar />
            <GoogleLogin
                render={renderProps => (
                    <RenderBox click={renderProps.onClick} dis={renderProps.disabled} />
                )}
                clientId="690992039442-7i1abco1ppl0t2b2qa40dc3c5nfe2m9e.apps.googleusercontent.com"
                scope={
                    "profile email https://www.googleapis.com/auth/calendar"
                }
                // buttonText="Iniciar sesión con Google"
                discoveryDocs={[
                    "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"
                ]}
                onSuccess={this.handleLoginSuccess}
                onFailure={this.handleLoginFailure}
            />
            </>
        )
    }
}

export default Login;