import { getUserFacultiesApi, getActiveUserApi } from './getUserFacultiesApi';
import { CONSULTAR_USUARIO_EMAIL, CONSULTAR_USUARIO_FACULTADES } from 'constants/Endpoints';
import { fetchUsuarioActivo } from '../../api/fetchApi';

export const getUserFacultiesActions = (dispatch, userProfile) => {
    const data = {email: userProfile};
    const consultarUsuario = {
		data,
		feature: 'CONSULTAR_USUARIO_EMAIL',
		endpoint: CONSULTAR_USUARIO_EMAIL,
        notification: false,
        port: 8181,
        ip: '35.184.246.116'
    };
    getUserFacultiesApi(dispatch, consultarUsuario);
};

export const getActiveUserActions = (dispatch, loginUser) => {
    const data = {
        claveUsuario: loginUser.claveUsuario,
        facultades:[{}],
        perfiles:[{}]
    };
    const consultarUsuarioActivo = {
		data,
		feature: 'CONSULTAR_USUARIO_FACULTADES',
		endpoint: CONSULTAR_USUARIO_FACULTADES,
        notification: false,
        port: 8181,
        ip: '35.184.246.116'
    };

    getActiveUserApi(dispatch, consultarUsuarioActivo);
};