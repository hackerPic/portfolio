const saveToken = token => {
    sessionStorage.setItem("token", token);
};

export default saveToken;
