import { fetchApi } from 'api/fetchApi';
import { setLoginUSer, setActiveUSer, setFacultyUSer, LOGIN_USER, ACTIVE_USER, FACULTY_USER } from 'actions/login.actions';

export const getUserFacultiesApi = async (dispatch, data) => {
	try {
        const response = await dispatch(fetchApi(data));
        dispatch(
            setActiveUSer(response)
        );
	} catch (error) {
	}
};

export const getActiveUserApi = async (dispatch, data) => {
    try {
        const response = await dispatch(fetchApi(data));
        dispatch(
            setFacultyUSer(response)
        );
	} catch (error) {
    }
};
