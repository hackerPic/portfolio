import React, { Fragment, Suspense } from 'react';
import { Route } from 'react-router-dom';
import DynamicContainer from 'util/DynamicContainer/DynamicContainer';

const Cobranza = ({ path, toMenu }) => {
	return (
		<Fragment>
			<Suspense fallback={<h1>Cargando...</h1>}>
				<Route
					exact
					strict
					path={`${path}/:subpage?`}
					render={({ match, history }) => (
						<DynamicContainer {...match} {...history} toMenu={toMenu} />
					)}
				/>
			</Suspense>
		</Fragment>
	);
};

export default Cobranza;
