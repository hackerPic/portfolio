import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';
import Quality from './../../../assets/img/quality.jpg';
import Experience from './../../../assets/img/experience.jpg';
import Businesswoman from './../../../assets/img/businesswoman.jpg';
import School from './../../../assets/img/school.jpg';
import Portfolio from './../../../assets/img/portfolio.jpg';
import User from './../../../assets/img/user.jpg';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { dataMiniCards } from './DashboardConstants';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

const images = [
    {
        url: User,
        title: 'Sobre mí',
        width: '16.5%',
    },
    {
        url: Experience,
        title: 'Skills',
        width: '16.5%',
    },
    {
        url: Businesswoman,
        title: 'Experiencia profesional',
        width: '16.5%',
    },
    {
        url: School,
        title: 'Academia',
        width: '16.5%',
    },
    {
        url: Portfolio,
        title: 'Portfolio',
        width: '16.5%',
    },
    {
        url: Quality,
        title: 'Recomendaciones',
        width: '16.5%',
    }
];

const useStylesCard = makeStyles({
    root: {
        minWidth: '350px',
        padding: '25px',
        backgroundColor: '#d4d4d4',
        borderRadius: '0px'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 16,
    },
    pos: {
        marginBottom: 12,
    },
});

const useStylesMiniCard = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        minWidth: 300,
        width: '100%',
    },
    image: {
        position: 'relative',
        height: 165,
        [theme.breakpoints.down('xs')]: {
            width: '100% !important', // Overrides inline-style
            height: 100,
        },
        '&:hover, &$focusVisible': {
            zIndex: 1,
            '& $imageBackdrop': {
                opacity: 0.15,
            },
            '& $imageMarked': {
                opacity: 0,
            },
            '& $imageTitle': {
                border: '4px solid currentColor',
            },
        },
    },
    focusVisible: {},
    imageButton: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: theme.palette.common.white,
    },
    imageSrc: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundSize: 'cover',
        backgroundPosition: 'center 40%',
    },
    imageBackdrop: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: theme.palette.common.black,
        opacity: 0.4,
        transition: theme.transitions.create('opacity'),
    },
    imageTitle: {
        position: 'relative',
        padding: `${theme.spacing(2)}px ${theme.spacing(4)}px ${theme.spacing(1) + 6}px`,
    },
    imageMarked: {
        height: 3,
        width: 18,
        backgroundColor: theme.palette.common.white,
        position: 'absolute',
        bottom: -2,
        left: 'calc(50% - 9px)',
        transition: theme.transitions.create('opacity'),
    },
}));

export const ButtonBases = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            {images.map((image) => (
                <ButtonBase
                    focusRipple
                    key={image.title}
                    className={classes.image}
                    focusVisibleClassName={classes.focusVisible}
                    style={{
                        width: image.width,
                    }}
                >
                    <span
                        className={classes.imageSrc}
                        style={{
                            backgroundImage: `url(${image.url})`,
                        }}
                    />
                    <span className={classes.imageBackdrop} />
                    <span className={classes.imageButton}>
                        <Typography
                            component="span"
                            variant="subtitle1"
                            color="inherit"
                            className={classes.imageTitle}
                        >
                            {image.title}
                            <span className={classes.imageMarked} />
                        </Typography>
                    </span>
                </ButtonBase>
            ))}
        </div>
    );
}

export const SimpleCard = () => {
    const classes = useStylesCard();
    const bull = <span className={classes.bullet}>•</span>;

    return (
        <Card className={classes.root}>
            <CardContent>
                <List component="nav" aria-label="Información personal">
                    <p className="fontPortfolio" style={{ fontSize: '30px' }}>
                        Información personal
                    </p>
                    <ListItem>
                        <ListItemText primary="Nombre" className="fontPortfolio" />
                        <ListItemIcon className="fontPortfolio">
                            Hannah Narváez
                        </ListItemIcon>
                    </ListItem>

                    <ListItem>
                        <ListItemText className="fontPortfolio" primary="Teléfono" />
                        <ListItemIcon className="fontPortfolio">
                            55 7425 9504
                        </ListItemIcon>
                    </ListItem>

                    <ListItem>
                        <ListItemText className="fontPortfolio" primary="e-mail" />
                        <ListItemIcon className="fontPortfolio">
                            hannah.n.kahnwald@gmail.com
                        </ListItemIcon>
                    </ListItem>

                    <ListItem>
                        <ListItemText className="fontPortfolio" primary="Edad" />
                        <ListItemIcon className="fontPortfolio">
                            37
                        </ListItemIcon>
                    </ListItem>

                </List>
            </CardContent>
        </Card>
    );
}

const FolderList = () => {
    const classes = useStylesMiniCard();

    return (
        <>
            {dataMiniCards.map(item => {
                const { icon, title, description } = item;
                return (
                    <Grid key={title} item xs={4}>
                        <List className={classes.root}>
                            <ListItem button>
                                <ListItemAvatar>
                                    {icon}
                                </ListItemAvatar>
                                <ListItemText
                                    disableTypography
                                    style={{ textAlign: 'justify' }}
                                    primary={title}
                                    secondary={description}
                                />
                            </ListItem>
                        </List>
                    </Grid>
                )
            })}
        </>
    );
}

export const QuickView = () => {
    return (
        <div className="spaceList">
            <Grid container spacing={0}>
                <Grid item xs={12}>
                    <h3 className="font-regular-light">
                        Habilidades y preferencias técnicas
                    </h3>
                </Grid>
                <Grid item xs={12}>
                    <h4 className="fontPortfolioSubTitle">
                        Habilidades técnicas
                    </h4>
                </Grid>
                <FolderList />
            </Grid>
        </div>
    )
};

export const Footer = () => {
    const useStylesFooter = makeStyles((theme) => ({
        root: {
          flexGrow: 1,
        },
        itemFooter: {
          padding: theme.spacing(2)
        },
    }));
    const classes = useStylesFooter();
    return(
        <div className="row" style={{ padding: '30px', backgroundColor: '#3e3d3d', color: 'white' }}>
            <Grid container direction="row" justify="space-between" alignItems="center" spacing={10}>
                <Grid className={classes.itemFooter} item md={3} xs={6}>
                    <List>
                        <ListItem>
                            <ListItemText primary="GRIDUS HTML TEMPLATE" secondary="The Gridus Resume Template has an unique modern flat intuitive design. You can choose one of 8 pre-defined Color schemes." />
                        </ListItem>
                    </List>
                </Grid>
                <Grid className={classes.itemFooter} item md={3} xs={6}>
                    <List>
                        <ListItem>
                            <ListItemText disableTypography primary="DESCARGAR CV" secondary={
                                <ButtonGroup disableElevation variant="contained" color="primary">
                                    <Button>Español</Button>
                                    <Button>Inglés</Button>
                                </ButtonGroup>} 
                            />
                        </ListItem>
                    </List>
                </Grid>
                <Grid className={classes.itemFooter} item md={3} xs={6}>
                    <List>
                        <ListItem>
                            <ListItemText primary="NEWSLETTER" secondary="Hola" />
                        </ListItem>
                    </List>
                </Grid>
                <Grid className={classes.itemFooter} item md={3} xs={6}>
                    <List>
                        <ListItem>
                            <ListItemText primary="SÍGUEME" secondary="Hola" />
                        </ListItem>
                    </List>
                </Grid>
            </Grid>
        </div>
    )
};