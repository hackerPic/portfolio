import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MemoryIcon from '@material-ui/icons/Memory';

const LinearProgressWithLabel = ({ number }) => {
    return (
        <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
                <LinearProgress variant="determinate" value={number} />
            </Box>
            <Box minWidth={35}>
                <Typography variant="body2" color="textSecondary">{number}</Typography>
            </Box>
        </Box>
    )
};

export const dataMiniCards = [
    { icon: <i style={{ fontSize: '3rem' }} className="devicon-confluence-plain colored"></i>, title: 'Diseño UI/UX', description: <LinearProgressWithLabel number={90} />},
    { icon: <i style={{ fontSize: '3rem' }} className="devicon-nodejs-plain-wordmark colored"></i>, title: 'BackEnd nodeJS', description: <LinearProgressWithLabel number={75} />},
    { icon: <MemoryIcon style={{ fontSize: '3rem' }} />, title: 'Ingeniería electrónica', description: <LinearProgressWithLabel number={80} />},
    { icon: <i className="devicon-git-plain-wordmark colored" style={{ fontSize: '3rem' }} ></i>, title: 'GIT', description: <LinearProgressWithLabel number={85} />},
    { icon: <i className="devicon-react-original-wordmark colored" style={{ fontSize: '3rem' }} ></i>, title: 'reactJS', description: <LinearProgressWithLabel number={90} />},
    { icon: <i class="devicon-linux-plain colored" style={{ fontSize: '3rem' }} ></i>, title: 'Linux', description: <LinearProgressWithLabel number={65} />},
    { icon: <i class="devicon-python-plain-wordmark colored" style={{ fontSize: '3rem' }} ></i>, title: 'Python puro', description: <LinearProgressWithLabel number={70} />},
    { icon: <i class="devicon-html5-plain-wordmark colored" style={{ fontSize: '3rem' }} ></i>, title: 'HTML 5', description: <LinearProgressWithLabel number={90} />},
    { icon: <i class="devicon-css3-plain-wordmark colored" style={{ fontSize: '3rem' }} ></i>, title: 'CSS 3', description: <LinearProgressWithLabel number={90} />}
];

export const rotate =  ['Diseño UX/UI','reactJS','diseño responsivo','nodeJS','Algoritmos'];

