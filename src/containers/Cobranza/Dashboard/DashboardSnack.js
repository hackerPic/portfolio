import React from 'react';
import SnackbarContent from '@material-ui/core/SnackbarContent';

const LongTextSnackbar = ({ messageRead, styler }) => {
    const stylus = { 
        borderRadius: '0px',
        paddingTop: '10px', 
        paddingBottom: '10px',
        boxShadow: '0px 0px 0px 0px rgba(0,0,0,0)',
        font: '28px/1.4 "Open Sans", sans-serif',
        textTransform: 'uppercase',
        fontSize: '28px',
        textRendering: 'optimizeLegibility',
        ...styler
    };
    return (
        <div>
            <SnackbarContent  
                style={stylus} 
                message={messageRead} 
            />
        </div>
    );
};

export default LongTextSnackbar;