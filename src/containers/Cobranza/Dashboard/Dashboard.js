import React, { Component, useEffect } from 'react';
import { connect } from 'react-redux';
import { getUserProfileState, getUserActiveState, getUserFacultyState } from 'selectors/loginSelector';
import Hannah from './../../../assets/img/hannah.jpeg';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import GetAppIcon from '@material-ui/icons/GetApp';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import { ButtonBases, SimpleCard, QuickView, Footer } from './DashboardUtils';
import LongTextSnackbar from './DashboardSnack';
import { rotate } from './DashboardConstants';

const rotateText = JSON.stringify(rotate);

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));
function CenteredGrid() {
    const classes = useStyles();

    useEffect(() => {
        var TxtRotate = function(el, toRotate, period) {
            this.toRotate = toRotate;
            this.el = el;
            this.loopNum = 0;
            this.period = parseInt(period, 10) || 2000;
            this.txt = '';
            this.tick();
            this.isDeleting = false;
          };
          
          TxtRotate.prototype.tick = function() {
            var i = this.loopNum % this.toRotate.length;
            var fullTxt = this.toRotate[i];
          
            if (this.isDeleting) {
              this.txt = fullTxt.substring(0, this.txt.length - 1);
            } else {
              this.txt = fullTxt.substring(0, this.txt.length + 1);
            }
          
            this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';
          
            var that = this;
            var delta = 300 - Math.random() * 100;
          
            if (this.isDeleting) { delta /= 2; }
          
            if (!this.isDeleting && this.txt === fullTxt) {
              delta = this.period;
              this.isDeleting = true;
            } else if (this.isDeleting && this.txt === '') {
              this.isDeleting = false;
              this.loopNum++;
              delta = 500;
            }
          
            setTimeout(function() {
              that.tick();
            }, delta);
          };
          

            var elements = document.getElementsByClassName('txt-rotate');
            for (var i=0; i<elements.length; i++) {
              var toRotate = elements[i].getAttribute('data-rotate');
              var period = elements[i].getAttribute('data-period');
              if (toRotate) {
                new TxtRotate(elements[i], JSON.parse(toRotate), period);
              }
            }
            // INJECT CSS
            var css = document.createElement("style");
            css.type = "text/css";
            css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
            document.body.appendChild(css);

    },[]);

    return (
        <>
            <div className='row'>
                <div className='col-md-3' style={{ paddingRight: '0px' }}>
                    <div className={classes.root}>
                        <Grid container spacing={0}>
                            <Grid item xs={12}>
                                <img src={Hannah} style={{ width: '100%' }} />
                            </Grid>
                        </Grid>
                    </div>
                </div>
                <div className='col-md-9' style={{ paddingLeft: '0px' }}>
                    <div className={classes.root}>
                        <Grid container spacing={0}>
                            <Grid item xs={11}>
                                <LongTextSnackbar styler={{ backgroundColor: '#656363' }} messageRead="Hannah Narváez Juárez" />
                            </Grid>
                            <Grid
                                container
                                direction="row"
                                justify="center"
                                alignItems="center"
                                item xs={1}
                                className="gridButton"
                                style={{ maxWidth: '71px' }}
                            >
                                <GetAppIcon style={{ fontSize: '2.5rem' }} />
                            </Grid>
                            <Grid item xs={11}>
                                <LongTextSnackbar 
                                    styler={{ backgroundColor: '#3e3d3d' }} 
                                    messageRead={
                                        <h1 style={{ color: 'white', marginBottom: '0em' }}> Experta en {' '}
                                            <span
                                                style={{ color: 'white' }}
                                                className="txt-rotate"
                                                data-period="1000"
                                                data-rotate={rotateText}
                                                
                                            />
                                        </h1>
                                    }
                                />
                            </Grid>
                            <Grid
                                container
                                className="gridButton"
                                direction="row"
                                justify="center"
                                alignItems="center"
                                item xs={1}
                                style={{ maxWidth: '71px' }}
                            >
                                <LinkedInIcon style={{ fontSize: '2.5rem' }} />
                            </Grid>
                            <Grid item xs={12}>
                                <ButtonBases />
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
            <div className='row'>
                <div className="col-md-8" style={{ paddingRight: '18px', backgroundColor: 'white' }}>
                    <div className={classes.root}>
                        <Grid container spacing={0}>
                            <Grid item xs={12}>
                                <QuickView />
                            </Grid>
                        </Grid>
                    </div>
                </div>
                <div className='col-md-4' style={{ padding: '0px 18px 0px 0px' }}>
                    <div className={classes.root}>
                        <Grid container spacing={0}>
                            <Grid item xs={12}>
                                <SimpleCard />
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}

@connect(store => ({
    loginUser: getUserProfileState(store),
    activeUser: getUserActiveState(store),
    facultyUser: getUserFacultyState(store),
}))
class Dashboard extends Component {
    render() {
        return (
            <CenteredGrid />
        )
    }
}

export default Dashboard;