import React from 'react';
import { Card, Icon, Avatar } from 'antd';

const { Meta } = Card;

const CardInfo = ({
	title,
	description,
	styleContent,
	header,
	footer,
	avatarImage,
	children,
	classTitle,
	styleTitle,
	size,
	...props
}) => {
	return (
		<Card
			size={size}
			{...props}
			style={styleContent}
			cover={
				header && (
					<img
						alt="example"
						src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
					/>
				)
			}
			actions={
				footer && [
					<Icon type="setting" key="setting" />,
					<Icon type="edit" key="edit" />,
					<Icon type="ellipsis" key="ellipsis" />,
				]
			}
		>
			<Meta
				avatar={
					avatarImage && (
						<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
					)
				}
				title={
					title && (
						<div style={{ ...styleTitle }} className={classTitle}>
							{title}
						</div>
					)
				}
				description={{ ...children }}
			/>
		</Card>
	);
};

export default CardInfo;
