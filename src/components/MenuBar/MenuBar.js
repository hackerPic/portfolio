import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
// import logo from '../../assets/img/';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import generalMenuBar from './../../constants/Menu';
import StarBorder from '@material-ui/icons/StarBorder';
import Collapse from '@material-ui/core/Collapse';
import Tooltip from '@material-ui/core/Tooltip';
import SubMenuBlocked from './SubMenuBlocked';

const drawerWidth = 380;
// if (open) {
//     return (
//         <div>
//             <Divider light />
//             <div className={classes.divider}><ListItemText primary={subheader} /></div>
//         </div>
//     )
// }

const useStyles = makeStyles((theme) => ({

    root: {
        display: 'flex',
    },
    divider: {
        width: '100%',
        fontWeight: 500,
        fontSize: '14px',
        paddingTop: '10px',
        lineHeight: '48px',
        paddingLeft: '16px',
        boxSizing: 'borderBox',
        color: 'rgba(0, 0, 0, 0.54)'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    nested: {
        paddingLeft: theme.spacing(6),
    }//#1a51ae
}));

export default function MenuBar() {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const [state, setState] = useState({
        subMenus: []
    });

    useEffect(() => {
        const subMenus = generalMenuBar.map(i => {
            if (i.routes.length !== 0) return { name: i.primaryText, value: false };
        }).filter(i => i !== undefined);
        setState({ ...state, subMenus });
    }, []);

    const HtmlTooltip = withStyles((theme) => ({
        tooltip: {
            minWidth: '30px',
            minHeight: '32px',
            padding: '15px 0px 15px 0px',
            color: '#fff',
            textAlign: 'left',
            textDecoration: 'none',
            wordWrap: 'break-word',
            backgroundColor: 'rgba(0,0,0,.65)',
            borderRadius: '2px',
            '-webkit-box-shadow': '0 3px 6px -4px rgba(0,0,0,.12), 0 6px 16px 0 rgba(0,0,0,.08), 0 9px 28px 8px rgba(0,0,0,.05)',
            boxShadow: '0 3px 6px -4px rgba(0,0,0,.12), 0 6px 16px 0 rgba(0,0,0,.08), 0 9px 28px 8px rgba(0,0,0,.05)',
            fontFamily: 'unset',
            fontStretch: 'expanded',
            font: 'caption'
        },
    }))(Tooltip);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
        let newArray = [...state.subMenus];
        const getMenu = newArray.map(i => {
            return {
                ...i,
                value: false
            }
        });
        setState({ ...state, subMenus: getMenu });
    };

    const handleClick = event => {
        let newArray = [...state.subMenus];
        const getMenu = newArray.findIndex(i => i.name === event);
        newArray[getMenu].value = !newArray[getMenu].value;
        setState({ ...state, subMenus: newArray });
    };

    const getItem = url => {
        console.log('url---', url);
    };

    const MenuMapper = () => {
        const getMenu = generalMenuBar.map((item, index) => {
            const { primaryText, parentPrimaryText, routes, url } = item;
            if (routes.length === 0) {
                if (!open) {
                    return (
                        <HtmlTooltip
                            key={primaryText}
                            title={
                                <div style={{
                                    padding: '0px 15px 0px 15px', 
                                    fontFamily: 'unset',
                                    fontStretch: 'expanded',
                                }}>{primaryText}</div>
                            }
                            placement="right"
                            interactive
                            arrow
                        >
                            <ListItem button key={primaryText}>
                                <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                                <ListItemText primary={primaryText} />
                            </ListItem>
                        </HtmlTooltip >
                    )
                } else {
                    return (
                        <ListItem key={primaryText} button key={primaryText}>
                            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                            <ListItemText primary={primaryText} />
                        </ListItem>
                    )
                }
            } else if (routes.length !== 0) {
                const getIndex = state.subMenus.findIndex(i => i.name === primaryText);
                const isName = getIndex !== -1 ? state.subMenus[getIndex].value : false;
                if (!open) {
                    return (
                        <>
                            <HtmlTooltip
                                key={primaryText}
                                title={
                                    <SubMenuBlocked 
                                        routes={routes} 
                                        valueSubMenu={routes} 
                                        getItem={(url) => getItem(url)} 
                                    />
                                }
                                placement="right"
                                interactive
                            >
                                <ListItem button name={primaryText} onClick={() => handleClick(primaryText)}>
                                    <ListItemIcon>
                                        <InboxIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={primaryText} />
                                    {isName ? <ExpandLess /> : <ExpandMore />}
                                </ListItem>
                            </HtmlTooltip >
                            <Collapse in={open && isName} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {routes.map(item => {
                                        if (item.url) {
                                            return (
                                                <ListItem key={item.primaryText} button className={classes.nested}>
                                                    <ListItemText primary={item.primaryText} />
                                                </ListItem>
                                            )
                                        }
                                    })}
                                </List>
                            </Collapse>
                        </>
                    )
                } else {
                    return (
                        <>
                            <ListItem key={primaryText} button name={primaryText} onClick={() => handleClick(primaryText)}>
                                <ListItemIcon>
                                    <InboxIcon />
                                </ListItemIcon>
                                <ListItemText primary={primaryText} />
                                {isName ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={isName} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {routes.map(item => {
                                        return (
                                            <ListItem key={item.primaryText} button className={classes.nested}>
                                                <ListItemText primary={item.primaryText} />
                                            </ListItem>
                                        )
                                    })}
                                </List>
                            </Collapse>
                        </>
                    )
                }

            }
        });
        return getMenu;
    };

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        Mini variant drawer
          </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
            >
                <div className={classes.toolbar}>
                    {/* <img src={logo} alt="image" /> */}
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </div>
                <Divider />
                <List>
                    <MenuMapper />
                    <Divider />
                </List>
            </Drawer>
        </div>
    );
}