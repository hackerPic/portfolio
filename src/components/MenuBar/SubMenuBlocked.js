import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import { getFileItem } from 'antd/lib/upload/utils';

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.2)',
            '&.MuiListItemText-primary': {
                color: theme.palette.common.white,
              },
        },
        paddingLeft: '20px'
    }
}))(MenuItem);

const SubMenuBlocked = ({
    routes,
    getItem
}) => {
    const [state, setState] = useState({
        listed: []
    });
    useEffect(() => {
        setState({ ...state, listed: routes });
    }, [routes]);

    const { listed } = state;
    return (
        <>
            {listed.map((item, index) => {
                const { primaryText, parentUrl } = item;
                const goToUrl = `${parentUrl}${routes[index].url}`;
                return (
                    <div key={parentUrl} onClick={() => getItem(goToUrl)}>
                        <StyledMenuItem key={primaryText} >
                            <ListItemText primary={primaryText} />
                        </StyledMenuItem>
                    </div>
                )
            })}
        </>
    );
}

export default SubMenuBlocked;