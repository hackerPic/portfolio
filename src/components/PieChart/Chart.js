import React from 'react';
import Pie from './Pie';

const App = props => {
	const { data } = props;

	const width = window.innerWidth;
	const height = window.innerHeight;
	const minViewportSize = Math.min(width, height);
	const radius = (minViewportSize * 0.4) / 2;
	const x = width / 2;
	const y = height / 2;

	return (
		<svg width="100%" height="100%">
			<Pie
				x={x}
				y={y}
				innerRadius={radius * 0.35}
				outerRadius={radius}
				cornerRadius={7}
				padAngle={0.02}
				data={data}
			/>
		</svg>
	);
};

export default App;
