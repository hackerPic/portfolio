import React from 'react';
import rd3 from 'react-d3-library';
import Slice from './Slice';

class Pie extends React.Component {
	constructor(props) {
		super(props);
		this.colorScale = rd3.scale.category10();
		this.renderSlice = this.renderSlice.bind(this);
	}
	renderSlice(value, i) {
		const { innerRadius, outerRadius, cornerRadius, padAngle } = this.props;
		return (
			<Slice
				key={i}
				innerRadius={innerRadius}
				outerRadius={outerRadius}
				cornerRadius={cornerRadius}
				padAngle={padAngle}
				value={value}
				label={value.data}
				fill={this.colorScale(i)}
			/>
		);
	}
	render() {
		const { x, y, data } = this.props;
		const pie = d3.layout.pie();
		return <g transform={`translate(${(x, y)})`}>{pie(data).map(this.renderSlice)}</g>;
	}
}

export default Pie;
