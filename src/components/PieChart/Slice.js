import React, { Component } from 'react';
import rd3 from 'react-d3-library';

class Slice extends Component {
	state = { isHovered: false };
	onMouseOver = () => {
		this.setState({ isHovered: true });
	};
	onMouseOut = () => {
		this.setState({ isHovered: false });
	};
	render() {
		const {
			value,
			label,
			fill,
			innerRadius = 0,
			outerRadius,
			cornerRadius,
			padAngle,
			...props
		} = this.props;
		let otherRadio = '';
		const { isHovered } = this.state;
		// eslint-disable-next-line no-const-assign
		if (isHovered) otherRadio = outerRadius *= 1.1;
		const arc = rd3.svg
			.arc()
			.innerRadius(innerRadius)
			.outerRadius(otherRadio)
			.cornerRadius(cornerRadius)
			.padAngle(padAngle);
		return (
			<g onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut} {...props}>
				<path d={arc(value)} fill={fill} />
				<text transform={translate(...arc.centroid(value))} dy=".35em" className="label">
					{label}
				</text>
			</g>
		);
	}
}

export default Slice;
