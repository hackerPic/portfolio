import { getDateFormat, currencyNumberToString } from 'util/Formatters';

const getFormat = data => {
	const { type } = data;
	let { value } = data;

	switch (type) {
		case 'currency':
			return currencyNumberToString(value);
		case 'date':
			return getDateFormat({ value });
		case 'none':
		default:
			return value;
	}
};

export const getCSV = (list, headers) => {
	let line = [];
	let str = '';

	// eslint-disable-next-line array-callback-return
	headers.map(h => {
		if (!h.ignore) {
			line.push(h.title);
		}
	});

	str += line.join(',');
	str += '\r\n';

	// eslint-disable-next-line array-callback-return
	list.map(l => {
		line = [];
		headers.map(header => {
			if (!header.ignore) {
				let text = l[header.dataIndex];

				if (typeof l[header.dataIndex] !== 'undefined') {
					if (typeof header.type !== 'undefined') {
						text = getFormat({ type: header.type, value: text });

						if (header.type === 'currency') {
							text = Number(text.replace(/[^0-9\.-]+/g, ''));
						}
					}
				} else {
					text = '';
				}

				line.push(text);
			}
		});

		str += line.join(',');
		str += '\r\n';
	});
	if (typeof str === 'string' && str.length !== 0) {
		return `data:text/csv;charset=utf-8,${encodeURIComponent(str)}`;
	}
	return str;
};

const exportCsv = async (str, title) => {
	let downloadLink = '';
	downloadLink = document.createElement('a');
	downloadLink.href = str;
	downloadLink.download = `${title}.csv`;

	document.body.appendChild(downloadLink);
	downloadLink.click();
	document.body.removeChild(downloadLink);
};

export const downloadCsv = async (uri, title = '') => {
	await exportCsv(uri, title);
};
