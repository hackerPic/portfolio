// PROPS Columns:
// 	columnFixed
// 	hasFilter
// 	sorter

/* eslint-disable lines-between-class-members */
/* eslint-disable no-multi-assign */
/* eslint-disable no-unused-expressions */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Table, Row, Icon, Input, Button, Menu, Dropdown } from 'antd';
import Highlighter from 'react-highlight-words';
import { TextField } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import SearchIcon from '@material-ui/icons/Search';
import { AccessAlarm, ThreeDRotation } from '@material-ui/icons';
import { getContentFormat } from 'util/Formatters';
import * as _ from 'lodash';
import { getCSV, downloadCsv } from './AntTableUtils';
import LongMenu from './AntTableSubMenu';

class AntTable extends Component {
	state = {
		headerData: [],
		tableData: [],
		searchText: '',
		searchedColumn: '',
		searchedTable: [],
		actionButtons: [],
		filteredTable: [],
		loading: false,
		isExact: false,
		anchorEl: null,
	};
	iconos = {
		accion1: <AccessAlarm />,
		acicon2: <ThreeDRotation />,
	};
	componentDidMount = () => {
		this.handleData();
		this.handleHeaders();
	};
	componentDidUpdate = prevProps => {
		const { data } = this.props;
		if (!_.isEqual(prevProps.data, data)) {
			this.handleDataUpdated(data);
		}
	};
	setActionButtons = () => {
		const { actionButtons } = this.state;
	};
	handleDataUpdated = data => {
		const { headers } = this.props;
		const newdata = data.map((item, index) => {
			this.handleTableCells(item, headers);
			const objeto = {
				...item,
				acciones: <LongMenu item={item} index={index} />,
			};
			return objeto;
		});
		this.setState({ searchedTable: newdata, filteredTable: newdata });
	};
	handleData = () => {
		const { headers, data } = this.props;
		const newdata = data.map((item, index) => {
			this.handleTableCells(item, headers);
			const objeto = {
				...item,
				acciones: <LongMenu item={item} index={index} />,
			};
			return objeto;
		});
		this.setState({ searchedTable: newdata, filteredTable: newdata });
	};
	formatter = key => {
		if (key && typeof key === 'number') {
			return key.toString();
		}
		if (key && typeof key === 'string') {
			return key.toLowerCase();
		}
	};
	// handleButtonSearch = value => {
	// 	this.handleLoading(true);
	// 	const query = this.formatter(value);
	// 	this.handleSetTableSearched(query);
	// };
	handleInputChange = e => {
		this.handleLoading(true);
		e.persist();
		const {
			target: { value },
		} = e;
		const query = this.formatter(value);
		this.handleSetTableSearched(query);
	};
	handleSetTableSearched = (query = '') => {
		const { searchedTable, isExact } = this.state;
		const result = [];
		searchedTable.filter(itExt => {
			let intObj = {};
			Object.values(itExt).map(itInt => {
				const type = typeof itInt;
				if (itInt && type === 'number') {
					if (!isExact) if (itInt.toString().includes(query)) intObj = { ...itExt };
					if (isExact) if (itInt.toString() === query) intObj = { ...itExt };
				} else if (itInt && type === 'string') {
					if (!isExact)
						if (itInt && itInt.toLowerCase().includes(query)) intObj = { ...itExt };
					if (isExact) if (itInt && itInt.toLowerCase() === query) intObj = { ...itExt };
				}
			});
			result.push(intObj);
		});
		const filteredTable = result.filter(i => Object.values(i).length !== 0);
		this.setState({ filteredTable });
		this.handleLoading(false);
	};
	handleLoading = loading => {
		this.setState({ loading });
	};
	handleClick = event => {
		this.setState({ anchorEl: event.currentTarget });
	};
	handleTableCells = (row, headers) => {
		const createOptions = [];

		headers.map(itemHead => {
			Object.keys(row).map(itemRow => {
				if (itemRow === itemHead.dataIndex) {
					const newObj = { content: row[itemRow] };
					createOptions.push(newObj);
					return createOptions;
				}
			});
			return createOptions;
		});

		const options = createOptions.map((op, i) => {
			let { content } = op;
			content = getContentFormat(content, headers[i]);
		});

		return options;
	};
	handleClose = () => {
		this.setState({ anchorEl: null });
	};
	handleMenuSearch = () => {
		this.setState(prevState => ({
			isExact: !prevState.isExact,
		}));
		this.handleClose();
	};
	handleClickExportar = async () => {
		const { headers, data } = this.props;
		const title = 'domiciliazion';
		const uri = await getCSV(data, headers);
		if (typeof uri === 'string' && uri.length !== 0) {
			await downloadCsv(uri, title);
		}
	};
	getTitle = () => {
		const { Search } = Input;
		const { loading, anchorEl, isExact } = this.state;
		const open = Boolean(anchorEl);
		return (
			<div style={{ paddingtop: '0px' }}>
				{/*	<div className="col-md-3">
					 <Menu
							id="fade-menu"
							anchorEl={anchorEl}
							keepMounted
							open={open}
							onClose={this.handleClose}
							TransitionComponent={Fade}
						>
							<MenuItem onClick={this.handleMenuSearch}>Búsqueda exacta</MenuItem>
						</Menu> 
				*/}

				<Grid
					container
					direction="row"
					justify="flex-end"
					alignItems="flex-end"
					spacing={1}
				>
					<Grid item xs={5}>
						<TextField
							onChange={this.handleInputChange}
							fullWidth
							style={{ margin: 0 }}
							id="searchTable"
							margin="normal"
							label="Buscar en la tabla"
						/>
					</Grid>
					<Grid item>
						<SearchIcon />
					</Grid>
					<Grid item xs={3} style={{ flexGrow: 0, maxWidth: 'none', flexBasis: 'auto' }}>
						<Button icon="cloud-download" onClick={this.handleClickExportar}>
							Descargar CSV
						</Button>
					</Grid>
				</Grid>
			</div>
		);
	};

	getColumnSearchProps = dataIndex => ({
		filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
			<div style={{ padding: 8 }}>
				<Input
					ref={node => {
						this.searchInput = node;
					}}
					placeholder={`Search ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
					onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
					style={{ width: 188, marginBottom: 8, display: 'block' }}
				/>
				<Button
					type="primary"
					onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
					icon="search"
					size="small"
					style={{ width: 90, marginRight: 8 }}
				>
					Search
				</Button>
				<Button
					onClick={() => this.handleReset(clearFilters)}
					size="small"
					style={{ width: 90 }}
				>
					Reset
				</Button>
			</div>
		),
		filterIcon: filtered => (
			<Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => this.searchInput.select());
			}
		},
		render: text =>
			this.state.searchedColumn === dataIndex ? (
				<Highlighter
					highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
					searchWords={[this.state.searchText]}
					autoEscape
					textToHighlight={text.toString()}
				/>
			) : (
				//  <AutoComplete
				//     dataSource={dataSource1}
				//     placeholder="Search name"
				//     value={selectedKeys[0]}
				//     onChange={value => setSelectedKeys(value ? [value] : [])}
				// />
				text
			),
	});
	handleHeaders = () => {
		const { headers, data } = this.props;
		const headerData = headers.map((itm, idx, arr) => {
			const { type = false } = itm;
			let object = {
				title: itm.title,
				width: idx === 0 ? 150 : '',
				dataIndex: itm.dataIndex ? itm.dataIndex : 'Index',
				sortDirections: ['descend', 'ascend'],
			};

			if (itm.columnFixed) {
				object.fixed = 'left';
			}
			//if (idx === arr.length - 1) object.fixed = 'right';
			if (itm.hasFilter && idx !== 0) {
				const getFilters = data.map(i => ({
					text: i[itm.dataIndex],
					value: i[itm.dataIndex],
				}));
				object.filters = Object.values(
					getFilters.reduce((a, c) => Object.assign(a, { [c.value]: c }), {})
				);
			}
			if (itm.hasFilter && idx !== 0) {
				object.onFilter = (value, record) => record[itm.dataIndex].indexOf(value) === 0;
				//object = { ...object, ...this.getColumnSearchProps(itm.dataIndex) };
			}
			if (itm.sorter && itm.type === 'currency')
				object.sorter = (a, b) => a[itm.dataIndex] - b[itm.dataIndex];
			return object;
		});
		this.setState({ headerData });
	};
	handleSearch = (selectedKeys, confirm, dataIndex) => {
		confirm();
		this.setState({
			searchText: selectedKeys[0],
			searchedColumn: dataIndex,
		});
	};

	handleReset = clearFilters => {
		clearFilters();
		this.setState({ searchText: '' });
	};
	handleMenu = () => {
		const { SubMenu } = Menu;
		return (
			<Menu>
				<Menu.Item>1st menu item</Menu.Item>
				<Menu.Item>2nd menu item</Menu.Item>
				<SubMenu title="sub menu">
					<Menu.Item>3rd menu item</Menu.Item>
					<Menu.Item>4th menu item</Menu.Item>
				</SubMenu>
				<SubMenu title="disabled sub menu" disabled>
					<Menu.Item>5d menu item</Menu.Item>
					<Menu.Item>6th menu item</Menu.Item>
				</SubMenu>
			</Menu>
		);
	};
	render() {
		const { headerData, filteredTable } = this.state;
		const { scrollY, headers, data, searchTable, scroll, ...rest } = this.props;
		const dataScroll = { y: `calc(100vh - ${scrollY})`, x: 1300 };
		return (
			<Table
				{...rest}
				columns={headerData}
				dataSource={filteredTable}
				title={searchTable ? this.getTitle : null}
				scroll={scroll ? dataScroll : false}
				tableLayout={undefined}
				bordered={false}
			/>
		);
	}
}

export default AntTable;
