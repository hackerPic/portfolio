const sendIFrameAdjust = () => {
	function onElementHeightChange(elm, callback) {
		let lastHeight = elm.clientHeight;
		let newHeight = '';
		(function run() {
			newHeight = elm.clientHeight;
			if (lastHeight !== newHeight) callback();
			lastHeight = newHeight;

			if (elm.onElementHeightChangeTimer) clearTimeout(elm.onElementHeightChangeTimer);

			elm.onElementHeightChangeTimer = setTimeout(run, 200);
		})();
	}

	onElementHeightChange(document.getElementById('container-microfront'), () => {
		const containerMicrofront = document.getElementById('container-microfront');
		const heightTotal = containerMicrofront.offsetHeight;
		const data = {
			MFmessengerObject: {
				iframeAdjust: heightTotal,
			},
		};
		parent.postMessage(data, '*');
	});
};

export default sendIFrameAdjust;
