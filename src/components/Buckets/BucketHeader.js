import React from 'react';

const BucketHeader = ({ headers }) => {
	const alignDetails = {
		textAlign: 'center',
	};
	return (
		<div
			className="left"
			style={{
				display: 'flex',
				flexDirection: 'row',
			}}
		>
			{headers.map(header => {
				const getStyle = header.key === 'detalles' ? alignDetails : null;
				return (
					<div style={getStyle} className="col-md fz-13 color-light" key={header.key}>
						<span>{header.label}</span>
					</div>
				);
			})}
		</div>
	);
};

export default BucketHeader;
