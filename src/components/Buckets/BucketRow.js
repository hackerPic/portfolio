import React from 'react';
import { Grid } from '@material-ui/core';
import { getContentFormat } from 'util/Formatters';
import './Bucket.css';

const bucketCell = (bucket, headers) => {
	const chunk = Object.values(bucket);
	const last = chunk.length - 1;
	const createBucketOptions = [];

	headers.map(itemHead => {
		Object.keys(bucket).map(itemRow => {
			if (itemRow === itemHead.key) {
				const newObj = { content: bucket[itemRow] };
				createBucketOptions.push(newObj);
				return createBucketOptions;
			}
		});
		return createBucketOptions;
	});

	const bucketCellOption = createBucketOptions.map(({ content }, i) => {
		const newContent = getContentFormat(content, headers[i]);
		const sm = headers[i].key === 'importe' ? 3 : (headers[i].key === 'detalles' && 2) || true;
		return (
			<Grid item xs={12} sm={sm} className="left" key={headers[i].key}>
				<p className={`g-m-0 ${last === i && 'center border-left'}`}>{newContent}</p>
			</Grid>
		);
	});
	return bucketCellOption;
};

const BucketRow = ({ fontSize = 14, bucketsData = [], headers = [] }) => {
	return (
		bucketsData.length !== 0 &&
		bucketsData.map((bucket, index) => {
			return (
				<Grid
					container
					spacing={1}
					alignItems="center"
					className={`left seguimiento-row g-mt-10 fz-${fontSize}`}
					key={index}
				>
					{bucketCell(bucket, headers)}
				</Grid>
			);
		})
	);
};

export default BucketRow;
