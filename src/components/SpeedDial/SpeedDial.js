import React from 'react';
import { Fab } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Switch from '@material-ui/core/Switch';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import FilterTiltShift from '@material-ui/icons/FileCopyOutlined';
import List from '@material-ui/icons/Save';
import Flag from '@material-ui/icons/Print';

const useStyles = makeStyles(theme => ({
	root: {
		transform: 'translateZ(0px)',
		flexGrow: 1,
	},
	exampleWrapper: {
		position: 'relative',
		//marginTop: theme.spacing(3),
		//height: 380,
	},
	radioGroup: {
		margin: theme.spacing(1, 0),
	},
	speedDial: {
		position: 'absolute',
		'&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
			bottom: theme.spacing(2),
			right: theme.spacing(2),
		},
		'&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
			top: theme.spacing(2),
			left: theme.spacing(2),
		},
	},
}));

const actions = [
	{
		icon: (
			<Fab
				aria-label="Icono uno"
				color="primary"
				size="small"
				style={{
					fontSize: '2rem',
				}}
			>
				<FilterTiltShift />
			</Fab>
		),
		name: 'Distribución',
	},
	{
		icon: (
			<Fab
				aria-label="Icono dos"
				color="primary"
				size="small"
				style={{
					fontSize: '2rem',
				}}
			>
				<List />
			</Fab>
		),
		name: 'Listas',
	},
	{
		icon: (
			<Fab
				aria-label="Icono tres"
				color="primary"
				size="small"
				style={{
					fontSize: '2rem',
				}}
			>
				<Flag />
			</Fab>
		),
		name: 'Rally',
	},
];

export default function SpeedDials() {
	const classes = useStyles();
	const [direction, setDirection] = React.useState('left');
	const [open, setOpen] = React.useState(false);
	const [hidden, setHidden] = React.useState(false);

	const handleDirectionChange = event => {
		setDirection(event.target.value);
	};

	const handleHiddenChange = event => {
		setHidden(event.target.checked);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleOpen = () => {
		setOpen(true);
	};

	return (
		<div className={classes.root}>
			{/* <FormLabel className={classes.radioGroup} component="legend">
				Direction
			</FormLabel>
			<RadioGroup
				aria-label="direction"
				name="direction"
				value={direction}
				onChange={handleDirectionChange}
				row
			>
				<FormControlLabel value="up" control={<Radio />} label="Up" />
				<FormControlLabel value="right" control={<Radio />} label="Right" />
				<FormControlLabel value="down" control={<Radio />} label="Down" />
				<FormControlLabel value="left" control={<Radio />} label="Left" />
			</RadioGroup> */}
			<div className={classes.exampleWrapper}>
				<SpeedDial
					ariaLabel="SpeedDial example"
					className={classes.speedDial}
					hidden={hidden}
					icon={<SpeedDialIcon />}
					onClose={handleClose}
					onOpen={handleOpen}
					open={open}
					direction={direction}
				>
					{actions.map(action => (
						<SpeedDialAction
							key={action.name}
							icon={action.icon}
							tooltipTitle={action.name}
							onClick={handleClose}
						/>
					))}
				</SpeedDial>
			</div>
		</div>
	);
}
