import React from 'react';
import { FormLabel } from '@material-ui/core';
import ImportantLabel from 'components/ImportantLabel/ImportantLabel';

const StatisticThreeLabels = ({ firstLabel = '', items = [], fontItems = {} }) => {
	return (
		<>
			<FormLabel
				style={{
					fontSize: '0.9rem',
					color: 'black',
				}}
				className="center left blue-gray-800"
				component="legend"
			>
				{firstLabel}
			</FormLabel>
			<div>
				<div className="row">
					{items.map((item, index) => {
						const { label, value } = item;
						const isCurrency = item.type === 'toCurrency' && true;
						// eslint-disable-next-line no-unused-expressions
						return (
							<div className="col-xs col-sm" key={index}>
								<ImportantLabel
									title={label || null}
									content={value || null}
									fullWidth
									toCurrency={isCurrency}
									contentStyle={fontItems}
								/>
							</div>
						);
					})}
				</div>
			</div>
		</>
	);
};

export default StatisticThreeLabels;
