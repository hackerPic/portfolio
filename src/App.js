import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import DynamicImport from 'constants/DynamicImport';
import { getUserProfileState, getUserActiveState, getUserFacultyState } from 'selectors/loginSelector';
import MenuBar from 'components/MenuBar/MenuBar';
import './App.css';
import 'primereact/resources/themes/nova-light/theme.css';
//import 'primereact/resources/themes/luna-amber/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import 'devicon/devicon-colors.css'
import 'devicon/devicon.min.css'

import Dashboard from './containers/Cobranza/Dashboard/Dashboard';

const GalileoProgress = DynamicImport('components/GalileoProgress/GalileoProgress');
const GalileoNotifications = DynamicImport('components/GalileoNotifications/GalileoNotifications');
const GalileoSnackBar = DynamicImport('components/GalileoSnackBar/GalileoSnackBar');
const Login = DynamicImport('containers/Login/Login');

@connect(store => ({
    loginUser: getUserProfileState(store),
    activeUser: getUserActiveState(store),
    facultyUser: getUserFacultyState(store),
}))
class App extends Component {
	state = {
		toMenu: false,
	};
	handlefromMenu = fromMenu => {
		this.setState({
			toMenu: fromMenu,
		});
	};
	render() {
		const { NODE_ENV } = process.env;
		const { toMenu } = this.state;
		return (
			<Router>
				<Fragment>
					<div style={{ padding: '20px 40px', backgroundColor: '#e2e2e2' }}>
						<Switch>
							<Route
								exact
								strict
								path="/"
								render={({ match }) => <Dashboard {...match} toMenu={toMenu} />}
							/>
						</Switch>
					</div>
					<GalileoProgress />
					<GalileoNotifications />
					<GalileoSnackBar />
				</Fragment>
			</Router>
		);
	}
}

export default App;
