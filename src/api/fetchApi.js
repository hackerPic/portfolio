//import axios from 'axios';
import { AUTORIZA_PERSONA_FACULTAD } from 'constants/Endpoints';
import galileoConfig from 'constants/galileoConfig';
import handleProgress from 'util/HandleProgress';
import { apiRequest, apiSuccess, apiError } from 'actions/api.actions';
//import { printTicketRequest, printTicketSuccess, printTicketError } from 'actions/app.actions';
import { hideNotification } from 'actions/notification.actions';
import handleNotification from 'util/HandleNotification';
import handleSnackbar from 'util/HandleSnackbar';

const handleCancel = (dispatch, feature) => {
	dispatch(hideNotification(feature));
};

const handleEndpoint = (endpoint, key, method, data, dispatch) => {
	if (method === 'POST') {
		const [, baseUrlName] = endpoint.split('/');
		return `http://${baseUrlName}.endpoints.${galileoConfig.BASE_URL_MSK}${endpoint}?key=AIzaSyCMPx5zQ0AjCyJv-wQLcRN8wHbN-MEHxzA`;
	}
	if (method === 'GET') {
		let en = endpoint;
		while (en.search('{') !== -1) {
			const [, variable] = en.split('{');
			const [variable2] = variable.split('}');
			en = en.replace(`{${variable2}}`, data[variable2]);
		}

		const [, baseUrlName] = endpoint.split('/');
		return `http://${baseUrlName}.endpoints.${galileoConfig.BASE_URL_MSK}${en}?key=${key}`;
	}
};

export const fetchUsuarioFacultades = ({
	ip,
	port,
	data,
	method = 'POST',
	params,
	feature,
	endpoint,
	notification = true,
} = {}) => async dispatch => {
	const url = `http://${ip || galileoConfig.LOGIN_URL}:${port ||
		galileoConfig.LOGIN_PORT}${endpoint}`;
	const headers = {
		'Content-Type': 'application/json',
	};
	handleProgress(dispatch, {
		show: true,
		feature: '[USUARIO FACULTADES]',
		message: 'Consultando facultades del usuario activo, espere un momento...',
	});
	dispatch(apiRequest(feature));
	try {
		const instance = await axios.request({
			url,
			method,
			data,
			params,
			headers,
		});
		const response = await instance.data.payload;
		dispatch(apiSuccess(feature));
		if (response.length === 0) {
			handleProgress(dispatch, {
				show: false,
				feature: '[USUARIO FACULTADES]',
			});
			handleNotification(dispatch, {
				type: 'error',
				content: 'Hubo un error al consultar las facultades del usuario activo',
				children: 'El usuario no tiene asignadas facultades para operar la caja.',
				okButtonTitle: 'Cerrar',
				onOk: () => handleCancel(dispatch, '[USUARIO FACULTADES]'),
				feature: '[USUARIO FACULTADES]',
				autoFocus: true,
			});
			return;
		}

		return await response;
	} catch (error) {
		dispatch(apiError(feature));
		if (notification) {
			handleProgress(dispatch, {
				show: false,
				feature: '[USUARIO FACULTADES]',
			});
		}
		handleNotification(dispatch, {
			type: 'error',
			content: 'Hubo un error al consultar las facultades del usuario activo',
			children: 'Se perdió la conexión con el servidor, inicie sesión nuevamente.',
			feature: '[USUARIO FACULTADES]',
			onOk: () => handleCancel(dispatch, '[USUARIO FACULTADES]'),
			okButtonTitle: 'Cerrar',
			autoFocus: true,
		});

		throw new Error(error);
	}
};
export const fetchMapeoEmpresas = ({
	ip,
	port,
	data,
	method = 'POST',
	params,
	feature,
	endpoint,
	notification = true,
} = {}) => async dispatch => {
	const url = `http://${ip || galileoConfig.LOGIN_URL}:${port ||
		galileoConfig.LOGIN_PORT}${endpoint}`;

	handleProgress(dispatch, {
		show: true,
		feature: '[MAPEO EMPRESAS]',
		message: 'Consultando status del usuario, espere un momento...',
	});
	dispatch(apiRequest('[MAPEO EMPRESAS]'));
	try {
		const instance = await fetch(url, {
			method,
			body: JSON.stringify(data),
			params,
			headers: {
				'Content-Type': 'application/json',
			},
		});
		if (instance.ok) {
			const response = await instance.json();
			const { payload = [] } = await response;
			dispatch(apiSuccess(feature));
			if (payload.length === 0) {
				handleProgress(dispatch, {
					show: false,
					feature: '[MAPEO EMPRESAS]',
				});
				handleNotification(dispatch, {
					type: 'error',
					content: 'Hubo un error con el usuario que ha iniciado sesión',
					children: 'El usuario no se encuentra activo, inicie sesión nuevamente.',
					okButtonTitle: 'Cerrar',
					onOk: () => handleCancel(dispatch),
					feature: '[MAPEO EMPRESAS]',
					autoFocus: true,
				});
				return;
			}
			const [mapeo] = await payload;
			return await mapeo.empresaOperativa;
		}
	} catch (error) {
		dispatch(apiError(feature));
		if (notification) {
			handleProgress(dispatch, {
				show: false,
				feature: '[MAPEO EMPRESAS]',
			});
		}
		handleNotification(dispatch, {
			type: 'error',
			content: 'Hubo un error con el usuario que ha iniciado sesión',
			children: 'Se perdió la conexión con el servidor, inicia sesión nuevamente.',
			feature: '[MAPEO EMPRESAS]',
			onOk: () => handleCancel(dispatch, '[MAPEO EMPRESAS]'),
			okButtonTitle: 'Cerrar',
			autoFocus: true,
		});
		throw new Error(error);
	}
};
export const fetchUsuarioActivo = ({
	ip,
	port,
	data,
	method = 'POST',
	params,
	feature,
	endpoint,
	notification = true,
} = {}) => async dispatch => {
	const url = `http://${ip || galileoConfig.LOGIN_URL}:${port ||
		galileoConfig.LOGIN_PORT}${endpoint}`;

	handleProgress(dispatch, {
		show: true,
		feature: '[USUARIO ACTIVO]',
		message: 'Consultando status del usuario, espere un momento...',
	});
	dispatch(apiRequest('[USUARIO ACTIVO]'));
	try {
		const instance = await fetch(url, {
			method,
			body: JSON.stringify(data),
			params,
			headers: {
				'Content-Type': 'application/json',
			},
		});
		dispatch(apiSuccess(feature));
		if (instance.ok && instance.status === 200) {
			const response = await instance.json();
			const { payload } = response;
			if (payload === 0) {
				handleProgress(dispatch, {
					show: false,
					feature: '[USUARIO ACTIVO]',
				});
				handleNotification(dispatch, {
					type: 'error',
					content: 'Hubo un error con el usuario que ha iniciado sesión',
					children: 'El usuario no se encuentra activo, inicie sesión nuevamente.',
					okButtonTitle: 'Cerrar',
					onOk: () => handleCancel(dispatch),
					feature: '[USUARIO ACTIVO]',
					autoFocus: true,
				});
				return;
			}
			const [user] = payload;
			return user;
		}
	} catch (error) {
		dispatch(apiError(feature));
		if (notification) {
			handleProgress(dispatch, {
				show: false,
				feature: '[USUARIO ACTIVO]',
			});
		}
		handleNotification(dispatch, {
			type: 'error',
			content: 'Hubo un error con el usuario que ha iniciado sesión',
			children: 'Se perdió la conexión con el servidor, inicie sesión nuevamente',
			feature: '[USUARIO ACTIVO]',
			onOk: () => handleCancel(dispatch, '[USUARIO ACTIVO]'),
			okButtonTitle: 'Cerrar',
			autoFocus: true,
		});
		throw new Error(error);
	}
};

export const fetchAuth = ({
	ip,
	port,
	data,
	method = 'POST',
	params,
	feature,
	endpoint = AUTORIZA_PERSONA_FACULTAD,
} = {}) => async dispatch => {
	const url = `http://${ip || galileoConfig.LOGIN_URL}:${port ||
		galileoConfig.LOGIN_PORT}${endpoint}`;
	const headers = {
		'Content-Type': 'application/json',
	};
	dispatch(apiRequest('[AUTORIZAR OPERACION]'));
	try {
		const instance = await axios.request({
			url,
			method,
			data,
			params,
			headers,
		});
		const { exito = false, personaAutoriza = '', mensaje = '' } = await instance.data.payload;
		dispatch(apiSuccess(feature));
		return { exito, personaAutoriza, mensaje };
	} catch (error) {
		dispatch(apiError(feature));
		handleProgress(dispatch, {
			show: false,
			feature: '[AUTORIZAR OPERACION]',
		});
		handleNotification(dispatch, {
			type: 'error',
			content: 'Hubo un error al autorizar la operación',
			children: 'Se perdió la conexión con el servidor.',
			feature: '[AUTORIZAR OPERACION]',
			onOk: () => handleCancel(dispatch, '[AUTORIZAR OPERACION]'),
			okButtonTitle: 'Cerrar',
			autoFocus: true,
		});
		throw new Error(error);
	}
};
export const fetchApi = ({
	ip,
	port,
	data = {},
	method = 'POST',
	params,
	feature,
	endpoint,
	notification = true,
	headers = true,
	key = '',
	notificationDisabled = true,
} = {}) => async dispatch => {
	let url = '';
	let newHeaders = '';

	if (headers) {
		newHeaders = {
			'Content-Type': 'application/json',
		};
	}
	if (!headers) {
		newHeaders = {};
	}
	if (endpoint.indexOf('msk/') > -1) {
		url = handleEndpoint(endpoint, key, method, data);
	} else {
		url = `http://${ip || galileoConfig.URL}:${port || galileoConfig.PORT}${endpoint}`;
	}

	if (notification) {
		handleProgress(dispatch, {
			show: true,
			feature,
		});
	}

	dispatch(apiRequest(feature));

	try {
		let instance;
		if (method === 'GET') {
			instance = await fetch(url, {
				method,
			});
		}
		if (method === 'POST') {
			instance = await fetch(url, {
				method,
				body: JSON.stringify(data),
				params,
				headers: newHeaders,
			});
		}
		dispatch(apiSuccess(feature));
		if (instance.ok) {
			const response = await instance.json();
			let { payload } = response;
			if (notification) {
				if (response.headerResponse.code === 204) {
					handleNotification(dispatch, {
						content: response.headerResponse.mensaje,
						okButtonTitle: 'Cerrar',
						onOk: () => handleCancel(dispatch, feature),
						feature,
						autoFocus: true,
					});
				}
				if (response.headerResponse.code === 409) {
					handleNotification(dispatch, {
						type: 'error',
						content: response.headerResponse.mensaje,
						children:
							response.headerResponse.uid && `UID: ${response.headerResponse.uid}`,
						okButtonTitle: 'Cerrar',
						onOk: () => handleCancel(dispatch, feature),
						feature,
						autoFocus: true,
					});
				}
				if (response.headerResponse.code === 200) {
					handleSnackbar(dispatch, {
						type: 'success',
						autoHideDuration: 3000,
						message: '¡Operación ejecutada correctamente!',
						feature,
					});
				}

				handleProgress(dispatch, {
					show: false,
					feature,
				});
			}
			if (!notification) {
				payload = {
					...response.headerResponse,
					...response.payload,
				};
			}
			return await payload;
		}
	} catch (error) {
		if (notificationDisabled) {
			dispatch(apiError(feature));
			handleProgress(dispatch, {
				show: false,
				feature,
			});
			handleNotification(dispatch, {
				body: data,
				type: 'error',
				content: 'Ocurrió un error al procesar la solicitud',
				children: 'Se perdió la conexión con el servidor.',
				feature,
				onOk: () => handleCancel(dispatch, feature),
				okButtonTitle: 'Cerrar',
				autoFocus: true,
				isBusy: false,
			});

			throw new Error(error);
		}
	}
};
export const fetchPrinter = query => async dispatch => {
	const { feature, base64, printerName } = query;
	// eslint-disable-next-line no-undef
	const configPrinter = qz.configs.create(printerName);
	const printData = [
		{
			type: 'raw',
			format: 'base64',
			data: base64,
			options: { language: 'ZPL' },
		},
	];
	const payload = {};
	dispatch(printTicketRequest(feature));
	// eslint-disable-next-line no-undef
	await qz
		.print(configPrinter, printData)
		.then(async () => {
			dispatch(printTicketSuccess(feature));
			payload.code = 200;
			payload.mensaje = 'Ticket impreso correctamente';
		})
		.catch(error => {
			payload.code = 500;
			payload.mensaje =
				'Hubo un error al imprimir el ticket, se perdió la conexión cn la impresora';
			dispatch(printTicketError(feature));
			throw new Error(error.error);
		});
	// eslint-disable-next-line no-return-await
	return await payload;
};
