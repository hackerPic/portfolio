import update from 'immutability-helper';
import { RESET_MESSAGES, SET_MESSAGES, ADD_CUSTOM_MESSAGES } from 'actions/messages.actions';
import { getMessagesSelector } from 'selectors/messagesSelector';

const initMessagesState = getMessagesSelector();

export default (state = initMessagesState, action) => {
	switch (true) {
		case action.type.includes(SET_MESSAGES):
			return update(state, {
				$set: action.payload,
			});
		case action.type.includes(ADD_CUSTOM_MESSAGES):
			return update(state, {
				$push: action.payload,
			});
		case action.type.includes(RESET_MESSAGES):
			return action.payload;
		default:
			return state;
	}
};
