import update from 'immutability-helper';
import { LOGIN_USER, ACTIVE_USER, FACULTY_USER } from 'actions/login.actions';
import { getLoginSelector } from 'selectors/loginSelector';

const initAppState = getLoginSelector();

export default (state = initAppState, action) => {
	switch (action.type) {
		case LOGIN_USER:
			return update(state, {
				userProfile: {
					$set: action.payload,
				},
			});
			case ACTIVE_USER:
						return update(state, {
				usuarioActivo: {
					$set: action.payload,
				},
			});
			case FACULTY_USER:
						return update(state, {
				usuarioFacultades: {
					$set: action.payload,
				},
			});
		default:
			return state;
	}
};
