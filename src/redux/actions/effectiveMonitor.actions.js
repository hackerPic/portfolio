// Action types
export const EFFECTIVE_MONITOR = 'EFFECTIVE_MONITOR';
export const RESET_EFFECTIVE_MONITOR = 'RESET_EFFECTIVE_MONITOR';
export const TOGGLE_EFFECTIVE_MONITOR = 'TOGGLE_EFFECTIVE_MONITOR';
export const GET_EFFECTIVE_MONITOR = 'GET_EFFECTIVE_MONITOR';
export const SET_EFFECTIVE_MONITOR = 'SET_EFFECTIVE_MONITOR';

// Action creators
export const resetEffectiveMonitor = payload => ({
	type: RESET_EFFECTIVE_MONITOR,
	payload,
});
export const getEffectiveMonitor = () => ({
	type: GET_EFFECTIVE_MONITOR,
});

export const toggleEffectiveMonitor = payload => ({
	type: TOGGLE_EFFECTIVE_MONITOR,
	payload,
});

export const setEffectiveMonitor = ({ payload }) => ({
	type: SET_EFFECTIVE_MONITOR,
	payload,
});
