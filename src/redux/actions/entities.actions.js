export const RESET_ENTITIES = 'RESET_ENTITIES';

export const resetEntities = payload => ({
	type: RESET_ENTITIES,
	payload,
});
