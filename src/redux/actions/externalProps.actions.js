import { SET_ACTION } from 'actions/getter.actions';

export const F2 = '[F2]';
export const GET_EXTERNAL_PROPS = `${F2} GET_EXTERNAL_PROPS`;

export const setExternalProps = payload => ({
	type: `${GET_EXTERNAL_PROPS} ${SET_ACTION}`,
	...{ ...payload },
	entity: {
		epic: 'externalProps',
		module: 'propsCaja',
	},
});
