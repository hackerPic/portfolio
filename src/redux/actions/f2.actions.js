import { SET_ACTION } from 'actions/getter.actions';

export const F2 = 'F2';
export const CONSULTAR_CATALOGO = `[${F2} CONSULTAR_CATALOGO]`;
export const CONSULTAR_SUBCATALOGOS = `[${F2} CONSULTAR_SUBCATALOGOS]`;
export const CONSULTAR_AGRUPAR_CONTRATOS = `[${F2} CONSULTAR_AGRUPAR_CONTRATOS]`;
export const CONSULTAR_AGRUPAR_FRECUENCIA = `[${F2} CONSULTAR_AGRUPAR_FRECUENCIA]`;

const entityF2Categorias = {
	epic: 'f2',
	module: 'categorias',
};
const entityF2SubCategoria = {
	epic: 'f2',
	module: 'subcategoria',
};
const entityF2contratosAgrupar = {
	epic: 'f2',
	module: 'contratosAgrupar',
};
const entityF2contratosAgruparFrecuencia = {
	epic: 'f2',
	module: 'contratosAgruparFrecuencia',
};

export const setEntityF2Categorias = ({ payload }) => ({
	type: `${CONSULTAR_CATALOGO} ${SET_ACTION}`,
	payload,
	entity: entityF2Categorias,
});

export const setEntityF2SubCategoria = ({ payload }) => ({
	type: `${CONSULTAR_SUBCATALOGOS} ${SET_ACTION}`,
	payload,
	entity: entityF2SubCategoria,
});
export const setEntityF2contratosAgrupar = ({ payload }) => ({
	type: `${CONSULTAR_AGRUPAR_CONTRATOS} ${SET_ACTION}`,
	payload,
	entity: entityF2contratosAgrupar,
});

export const setEntityF2ContratosAgruparFrecuencia = ({ payload }) => ({
	type: `${CONSULTAR_AGRUPAR_FRECUENCIA} ${SET_ACTION}`,
	payload,
	entity: entityF2contratosAgruparFrecuencia,
});
