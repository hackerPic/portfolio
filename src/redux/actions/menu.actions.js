// Action types
export const GALILEO = '[GALILEO]';
export const MENU = 'MENU';
export const SUBMENU = 'SUBMENU';
export const GET_MAIN_MENU = `GET_MAIN_${MENU}`;
export const SET_MAIN_MENU = `SET_MAIN_${MENU}`;
export const GET_SUB_MENU = `SET_${SUBMENU}`;
export const SET_SUB_MENU = `SET_${SUBMENU}`;

// Action creators
export const getMainMenu = () => ({
	type: `${GALILEO} ${GET_MAIN_MENU}`,
});
export const setMainMenu = payload => ({
	type: `${GALILEO} ${SET_MAIN_MENU}`,
	payload,
});
export const setSubMenu = () => ({
	type: `${GALILEO} ${SET_SUB_MENU}`,
	payload: [],
});
