export const MESSAGES = 'MESSAGES';
export const RESET_MESSAGES = `[${MESSAGES} RESET]`;
export const SET_MESSAGES = `[${MESSAGES} set messages]`;
export const ADD_CUSTOM_MESSAGES = `[${MESSAGES} add custom messages]`;
export const SHOW_MESSAGES = `[${MESSAGES} show] `;
export const HIDE_MESSAGES = `[${MESSAGES} hide]`;

export const resetMessages = payload => ({
	type: RESET_MESSAGES,
	payload,
});
export const setMessages = payload => ({
	type: SET_MESSAGES,
	payload,
});
export const addCustomMessages = payload => ({
	type: ADD_CUSTOM_MESSAGES,
	payload,
});
export const showMessages = () => ({
	type: SHOW_MESSAGES,
	payload: true,
});
export const hideMessages = () => ({
	type: HIDE_MESSAGES,
});
