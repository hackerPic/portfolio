/* Action types */
export const LOGIN_USER = '[LOGIN_USER]';
export const ACTIVE_USER = '[ACTIVE_USER]';
export const FACULTY_USER = '[FACULTY_USER]';

export const setLoginUSer = payload => ({
	type: LOGIN_USER,
	payload: payload,
});

export const setActiveUSer = payload => ({
	type: ACTIVE_USER,
	payload: payload,
});

export const setFacultyUSer = payload => ({
	type: FACULTY_USER,
	payload: payload,
});
