import app from './app.entity';
import ui from './ui.entity';
import notification from './notification.entity';
import snackbar from './snackbar.entity';
import entities from './entities';
import reset from './reset.entity';
import loginUser from './login.entity';

export default {
	app,
	entities,
	notification,
	snackbar,
	ui,
	reset,
	loginUser,
};
