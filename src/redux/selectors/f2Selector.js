import { createSelector } from 'reselect';

const f2CategoriaState = state => state.entities.f2.categorias;
const f2SubCategoriaState = state => state.entities.f2.subcategoria;
const f2CcontratosAgrupar = state => state.entities.f2.contratosAgrupar;
const f2ContratosAgruparFrecuencia = state => state.entities.f2.contratosAgruparFrecuencia;

// Reselect functions
export const getF2CategoriaState = createSelector(
	[f2CategoriaState],
	categorias => categorias
);
export const getF2SubCategoriaState = createSelector(
	[f2SubCategoriaState],
	subcategoria => subcategoria
);
export const getF2CcontratosAgrupar = createSelector(
	[f2CcontratosAgrupar],
	contratosAgrupar => contratosAgrupar
);
export const getF2ContratosAgruparFrecuencia = createSelector(
	[f2ContratosAgruparFrecuencia],
	contratosAgruparFrecuencia => contratosAgruparFrecuencia
);
