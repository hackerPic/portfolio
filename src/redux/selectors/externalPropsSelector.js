import { createSelector } from 'reselect';

const getExternalProps = state => state.entities.externalProps.propsCaja;

const getExternalPropsState = createSelector(
	[getExternalProps],
	externalProps => externalProps
);

export default getExternalPropsState;
