import { createSelector } from 'reselect';
import store from '../store/index';

// Selectors
export const getMessagesSelector = () => store.messages;
const getMessages = state => state.messages;

// Reselect functions
export const getMessagesState = createSelector(
	[getMessages],
	messages => messages
);
