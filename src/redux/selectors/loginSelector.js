import { createSelector } from 'reselect';
import store from '../store/index';

// Selectors
const getUserProfile = state => state.login.userProfile;
const getUserActive = state => state.login.usuarioActivo;
const getUserFaculty = state => state.login.usuarioFacultades;
export const getLoginSelector = () => store.loginUser;

// Reselect functions
export const getUserProfileState = createSelector(
	[getUserProfile],
	userProfile => userProfile
);

export const getUserActiveState = createSelector(
	[getUserActive],
	userActive => userActive
);

export const getUserFacultyState = createSelector(
	[getUserFaculty],
	userFaculty => userFaculty
);
